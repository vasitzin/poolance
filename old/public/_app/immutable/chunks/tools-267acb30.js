import{S as x,i as $,s as ee,B,k as U,a as q,l as V,m as P,c as K,h as S,n as I,L as b,b as G,F as O,G as re,C as F,D as W,E as j,Q as ae,f as M,t as N,R as Rt,T as Ot,q as pe,r as ve,u as Ae,_ as be,$ as at,H as z,a0 as Ft,I as ze,a1 as ot,a2 as ls,U as X,P as cs,v as Wt,w as jt,x as zt,y as Gt,a3 as on,g as qt,d as Kt,Z as ln,p as Dt,V as Mt,a4 as hi,a5 as Me,K as _i,e as cn,M as us,N as mi,O as gi,a6 as pi,a7 as un,A as Z}from"./index-b4da0c52.js";const vi={color:"currentColor",class:"",opacity:.1,centered:!1,spreadingDuration:".4s",spreadingDelay:"0s",spreadingTimingFunction:"linear",clearingDuration:"1s",clearingDelay:"0s",clearingTimingFunction:"ease-in-out"};function dn(t,e={}){t.stopImmediatePropagation();const n={...vi,...e},s=t.touches?!!t.touches[0]:!1,i=s?t.touches[0].currentTarget:t.currentTarget,r=document.createElement("div"),a=r.style;r.className=`material-ripple ${n.class}`,a.position="absolute",a.color="inherit",a.borderRadius="50%",a.pointerEvents="none",a.width="100px",a.height="100px",a.marginTop="-50px",a.marginLeft="-50px",i.appendChild(r),a.opacity=n.opacity,a.transition=`transform ${n.spreadingDuration} ${n.spreadingTimingFunction} ${n.spreadingDelay},opacity ${n.clearingDuration} ${n.clearingTimingFunction} ${n.clearingDelay}`,a.transform="scale(0) translate(0,0)",a.background=n.color;const o=i.getBoundingClientRect();if(n.centered)a.top=`${o.height/2}px`,a.left=`${o.width/2}px`;else{const l=s?t.touches[0].clientY:t.clientY,c=s?t.touches[0].clientX:t.clientX;a.top=`${l-o.top}px`,a.left=`${c-o.left}px`}return a.transform=`scale(${Math.max(o.width,o.height)*.02}) translate(0,0)`,r}function yi(t){t&&(t.addEventListener("transitionend",e=>{e.propertyName==="opacity"&&t.remove()}),t.style.opacity=0)}const ds=(t,e={})=>{let n=e,s=!1,i,r=!1;const a=m=>{i=dn(m,n)},o=()=>yi(i),l=m=>{!r&&(m.keyCode===13||m.keyCode===32)&&(i=dn(m,{...n,centered:!0}),r=!0)},c=()=>{r=!1,o()};function u(){t.classList.add("s-ripple-container"),t.addEventListener("pointerdown",a),t.addEventListener("pointerup",o),t.addEventListener("pointerleave",o),t.addEventListener("keydown",l),t.addEventListener("keyup",c),s=!1}function f(){t.classList.remove("s-ripple-container"),t.removeEventListener("pointerdown",a),t.removeEventListener("pointerup",o),t.removeEventListener("pointerleave",o),t.removeEventListener("keydown",l),t.removeEventListener("keyup",c),s=!0}return n&&u(),{update(m){n=m,n&&s?u():n||s||f()},destroy:f}};function fn(t){return typeof t=="number"?`${t}px`:t}const fs=(t,e)=>{let n=e;return Object.entries(n).forEach(([s,i])=>{i&&t.style.setProperty(`--s-${s}`,fn(i))}),{update(s){Object.entries(s).forEach(([i,r])=>{r&&(t.style.setProperty(`--s-${i}`,fn(r)),delete n[i])}),Object.keys(n).forEach(i=>t.style.removeProperty(`--s-${i}`)),n=s}}};function hn(t){let e,n,s,i=t[10]&&_n(t);return{c(){e=Rt("svg"),n=Rt("path"),i&&i.c(),this.h()},l(r){e=Ot(r,"svg",{xmlns:!0,width:!0,height:!0,viewBox:!0});var a=P(e);n=Ot(a,"path",{d:!0});var o=P(n);i&&i.l(o),o.forEach(S),a.forEach(S),this.h()},h(){I(n,"d",t[9]),I(e,"xmlns","http://www.w3.org/2000/svg"),I(e,"width",t[0]),I(e,"height",t[1]),I(e,"viewBox",s="0 0 "+t[4]+" "+t[5])},m(r,a){G(r,e,a),O(e,n),i&&i.m(n,null)},p(r,a){r[10]?i?i.p(r,a):(i=_n(r),i.c(),i.m(n,null)):i&&(i.d(1),i=null),a&512&&I(n,"d",r[9]),a&1&&I(e,"width",r[0]),a&2&&I(e,"height",r[1]),a&48&&s!==(s="0 0 "+r[4]+" "+r[5])&&I(e,"viewBox",s)},d(r){r&&S(e),i&&i.d()}}}function _n(t){let e,n;return{c(){e=Rt("title"),n=pe(t[10])},l(s){e=Ot(s,"title",{});var i=P(e);n=ve(i,t[10]),i.forEach(S)},m(s,i){G(s,e,i),O(e,n)},p(s,i){i&1024&&Ae(n,s[10])},d(s){s&&S(e)}}}function bi(t){let e,n,s,i,r,a,o,l=t[9]&&hn(t);const c=t[13].default,u=B(c,t,t[12],null);return{c(){e=U("i"),l&&l.c(),n=q(),u&&u.c(),this.h()},l(f){e=V(f,"I",{"aria-hidden":!0,class:!0,"aria-label":!0,"aria-disabled":!0,style:!0});var m=P(e);l&&l.l(m),n=K(m),u&&u.l(m),m.forEach(S),this.h()},h(){I(e,"aria-hidden","true"),I(e,"class",s="s-icon "+t[2]),I(e,"aria-label",t[10]),I(e,"aria-disabled",t[8]),I(e,"style",t[11]),b(e,"spin",t[7]),b(e,"disabled",t[8])},m(f,m){G(f,e,m),l&&l.m(e,null),O(e,n),u&&u.m(e,null),r=!0,a||(o=re(i=fs.call(null,e,{"icon-size":t[3],"icon-rotate":`${t[6]}deg`})),a=!0)},p(f,[m]){f[9]?l?l.p(f,m):(l=hn(f),l.c(),l.m(e,n)):l&&(l.d(1),l=null),u&&u.p&&(!r||m&4096)&&F(u,c,f,f[12],r?j(c,f[12],m,null):W(f[12]),null),(!r||m&4&&s!==(s="s-icon "+f[2]))&&I(e,"class",s),(!r||m&1024)&&I(e,"aria-label",f[10]),(!r||m&256)&&I(e,"aria-disabled",f[8]),(!r||m&2048)&&I(e,"style",f[11]),i&&ae(i.update)&&m&72&&i.update.call(null,{"icon-size":f[3],"icon-rotate":`${f[6]}deg`}),(!r||m&132)&&b(e,"spin",f[7]),(!r||m&260)&&b(e,"disabled",f[8])},i(f){r||(M(u,f),r=!0)},o(f){N(u,f),r=!1},d(f){f&&S(e),l&&l.d(),u&&u.d(f),a=!1,o()}}}function Ii(t,e,n){let{$$slots:s={},$$scope:i}=e,{class:r=""}=e,{size:a="24px"}=e,{width:o=a}=e,{height:l=a}=e,{viewWidth:c="24"}=e,{viewHeight:u="24"}=e,{rotate:f=0}=e,{spin:m=!1}=e,{disabled:d=!1}=e,{path:h=null}=e,{label:y=null}=e,{style:g=null}=e;return t.$$set=p=>{"class"in p&&n(2,r=p.class),"size"in p&&n(3,a=p.size),"width"in p&&n(0,o=p.width),"height"in p&&n(1,l=p.height),"viewWidth"in p&&n(4,c=p.viewWidth),"viewHeight"in p&&n(5,u=p.viewHeight),"rotate"in p&&n(6,f=p.rotate),"spin"in p&&n(7,m=p.spin),"disabled"in p&&n(8,d=p.disabled),"path"in p&&n(9,h=p.path),"label"in p&&n(10,y=p.label),"style"in p&&n(11,g=p.style),"$$scope"in p&&n(12,i=p.$$scope)},t.$$.update=()=>{t.$$.dirty&8&&(n(0,o=a),n(1,l=a))},[o,l,r,a,c,u,f,m,d,h,y,g,i,s]}class Ei extends x{constructor(e){super(),$(this,e,Ii,bi,ee,{class:2,size:3,width:0,height:1,viewWidth:4,viewHeight:5,rotate:6,spin:7,disabled:8,path:9,label:10,style:11})}}const wi=t=>t.filter(e=>!!e),It=t=>t.split(" ").filter(e=>!!e),Jt=(t,e)=>{let n=e;return t.classList.add(...It(wi(n).join(" "))),{update(s){const i=s;i.forEach((r,a)=>{r?t.classList.add(...It(r)):n[a]&&t.classList.remove(...It(n[a]))}),n=i}}};function Ti(t){let e,n,s,i,r,a,o,l;const c=t[19].default,u=B(c,t,t[18],null);let f=[{class:s="s-btn size-"+t[5]+" "+t[1]},{type:t[14]},{style:t[16]},{disabled:t[11]},{"aria-disabled":t[11]},t[17]],m={};for(let d=0;d<f.length;d+=1)m=be(m,f[d]);return{c(){e=U("button"),n=U("span"),u&&u.c(),this.h()},l(d){e=V(d,"BUTTON",{class:!0,type:!0,style:!0,"aria-disabled":!0});var h=P(e);n=V(h,"SPAN",{class:!0});var y=P(n);u&&u.l(y),y.forEach(S),h.forEach(S),this.h()},h(){I(n,"class","s-btn__content"),at(e,m),b(e,"s-btn--fab",t[2]),b(e,"icon",t[3]),b(e,"block",t[4]),b(e,"tile",t[6]),b(e,"text",t[7]||t[3]),b(e,"depressed",t[8]||t[7]||t[11]||t[9]||t[3]),b(e,"outlined",t[9]),b(e,"rounded",t[10]),b(e,"disabled",t[11])},m(d,h){G(d,e,h),O(e,n),u&&u.m(n,null),e.autofocus&&e.focus(),t[21](e),a=!0,o||(l=[re(i=Jt.call(null,e,[t[12]&&t[13]])),re(r=ds.call(null,e,t[15])),z(e,"click",t[20])],o=!0)},p(d,[h]){u&&u.p&&(!a||h&262144)&&F(u,c,d,d[18],a?j(c,d[18],h,null):W(d[18]),null),at(e,m=Ft(f,[(!a||h&34&&s!==(s="s-btn size-"+d[5]+" "+d[1]))&&{class:s},(!a||h&16384)&&{type:d[14]},(!a||h&65536)&&{style:d[16]},(!a||h&2048)&&{disabled:d[11]},(!a||h&2048)&&{"aria-disabled":d[11]},h&131072&&d[17]])),i&&ae(i.update)&&h&12288&&i.update.call(null,[d[12]&&d[13]]),r&&ae(r.update)&&h&32768&&r.update.call(null,d[15]),b(e,"s-btn--fab",d[2]),b(e,"icon",d[3]),b(e,"block",d[4]),b(e,"tile",d[6]),b(e,"text",d[7]||d[3]),b(e,"depressed",d[8]||d[7]||d[11]||d[9]||d[3]),b(e,"outlined",d[9]),b(e,"rounded",d[10]),b(e,"disabled",d[11])},i(d){a||(M(u,d),a=!0)},o(d){N(u,d),a=!1},d(d){d&&S(e),u&&u.d(d),t[21](null),o=!1,ze(l)}}}function ki(t,e,n){const s=["class","fab","icon","block","size","tile","text","depressed","outlined","rounded","disabled","active","activeClass","type","ripple","style","button"];let i=ot(e,s),{$$slots:r={},$$scope:a}=e,{class:o=""}=e,{fab:l=!1}=e,{icon:c=!1}=e,{block:u=!1}=e,{size:f="default"}=e,{tile:m=!1}=e,{text:d=!1}=e,{depressed:h=!1}=e,{outlined:y=!1}=e,{rounded:g=!1}=e,{disabled:p=null}=e,{active:A=!1}=e,{activeClass:k="active"}=e,{type:D="button"}=e,{ripple:v={}}=e,{style:L=null}=e,{button:w=null}=e;function _(C){X.call(this,t,C)}function E(C){cs[C?"unshift":"push"](()=>{w=C,n(0,w)})}return t.$$set=C=>{e=be(be({},e),ls(C)),n(17,i=ot(e,s)),"class"in C&&n(1,o=C.class),"fab"in C&&n(2,l=C.fab),"icon"in C&&n(3,c=C.icon),"block"in C&&n(4,u=C.block),"size"in C&&n(5,f=C.size),"tile"in C&&n(6,m=C.tile),"text"in C&&n(7,d=C.text),"depressed"in C&&n(8,h=C.depressed),"outlined"in C&&n(9,y=C.outlined),"rounded"in C&&n(10,g=C.rounded),"disabled"in C&&n(11,p=C.disabled),"active"in C&&n(12,A=C.active),"activeClass"in C&&n(13,k=C.activeClass),"type"in C&&n(14,D=C.type),"ripple"in C&&n(15,v=C.ripple),"style"in C&&n(16,L=C.style),"button"in C&&n(0,w=C.button),"$$scope"in C&&n(18,a=C.$$scope)},[w,o,l,c,u,f,m,d,h,y,g,p,A,k,D,v,L,i,a,r,_,E]}class lc extends x{constructor(e){super(),$(this,e,ki,Ti,ee,{class:1,fab:2,icon:3,block:4,size:5,tile:6,text:7,depressed:8,outlined:9,rounded:10,disabled:11,active:12,activeClass:13,type:14,ripple:15,style:16,button:0})}}function Ci(t){return t.split(" ").map(e=>/^(lighten|darken|accent)-/.test(e)?`text-${e}`:`${e}-text`)}function mn(t,e){if(/^(#|rgb|hsl|currentColor)/.test(e))return t.style.color=e,!1;if(e.startsWith("--"))return t.style.color=`var(${e})`,!1;const n=Ci(e);return t.classList.add(...n),n}const Si=(t,e)=>{let n;return typeof e=="string"&&(n=mn(t,e)),{update(s){n?t.classList.remove(...n):t.style.color=null,typeof s=="string"&&(n=mn(t,s))}}};const Ai=t=>({}),gn=t=>({}),Li=t=>({}),pn=t=>({}),Ri=t=>({}),vn=t=>({});function Oi(t){let e,n,s,i,r,a,o,l,c,u,f,m;const d=t[9]["prepend-outer"],h=B(d,t,t[8],vn),y=t[9].default,g=B(y,t,t[8],null),p=t[9].messages,A=B(p,t,t[8],pn),k=t[9]["append-outer"],D=B(k,t,t[8],gn);return{c(){e=U("div"),h&&h.c(),n=q(),s=U("div"),i=U("div"),g&&g.c(),r=q(),a=U("div"),A&&A.c(),o=q(),D&&D.c(),this.h()},l(v){e=V(v,"DIV",{class:!0,style:!0});var L=P(e);h&&h.l(L),n=K(L),s=V(L,"DIV",{class:!0});var w=P(s);i=V(w,"DIV",{class:!0});var _=P(i);g&&g.l(_),_.forEach(S),r=K(w),a=V(w,"DIV",{class:!0});var E=P(a);A&&A.l(E),E.forEach(S),w.forEach(S),o=K(L),D&&D.l(L),L.forEach(S),this.h()},h(){I(i,"class","s-input__slot"),I(a,"class","s-input__details"),I(s,"class","s-input__control"),I(e,"class",l="s-input "+t[0]),I(e,"style",t[7]),b(e,"dense",t[2]),b(e,"error",t[5]),b(e,"success",t[6]),b(e,"readonly",t[3]),b(e,"disabled",t[4])},m(v,L){G(v,e,L),h&&h.m(e,null),O(e,n),O(e,s),O(s,i),g&&g.m(i,null),O(s,r),O(s,a),A&&A.m(a,null),O(e,o),D&&D.m(e,null),u=!0,f||(m=re(c=Si.call(null,e,t[6]?"success":t[5]?"error":t[1])),f=!0)},p(v,[L]){h&&h.p&&(!u||L&256)&&F(h,d,v,v[8],u?j(d,v[8],L,Ri):W(v[8]),vn),g&&g.p&&(!u||L&256)&&F(g,y,v,v[8],u?j(y,v[8],L,null):W(v[8]),null),A&&A.p&&(!u||L&256)&&F(A,p,v,v[8],u?j(p,v[8],L,Li):W(v[8]),pn),D&&D.p&&(!u||L&256)&&F(D,k,v,v[8],u?j(k,v[8],L,Ai):W(v[8]),gn),(!u||L&1&&l!==(l="s-input "+v[0]))&&I(e,"class",l),(!u||L&128)&&I(e,"style",v[7]),c&&ae(c.update)&&L&98&&c.update.call(null,v[6]?"success":v[5]?"error":v[1]),(!u||L&5)&&b(e,"dense",v[2]),(!u||L&33)&&b(e,"error",v[5]),(!u||L&65)&&b(e,"success",v[6]),(!u||L&9)&&b(e,"readonly",v[3]),(!u||L&17)&&b(e,"disabled",v[4])},i(v){u||(M(h,v),M(g,v),M(A,v),M(D,v),u=!0)},o(v){N(h,v),N(g,v),N(A,v),N(D,v),u=!1},d(v){v&&S(e),h&&h.d(v),g&&g.d(v),A&&A.d(v),D&&D.d(v),f=!1,m()}}}function Di(t,e,n){let{$$slots:s={},$$scope:i}=e,{class:r=""}=e,{color:a=null}=e,{dense:o=!1}=e,{readonly:l=!1}=e,{disabled:c=!1}=e,{error:u=!1}=e,{success:f=!1}=e,{style:m=null}=e;return t.$$set=d=>{"class"in d&&n(0,r=d.class),"color"in d&&n(1,a=d.color),"dense"in d&&n(2,o=d.dense),"readonly"in d&&n(3,l=d.readonly),"disabled"in d&&n(4,c=d.disabled),"error"in d&&n(5,u=d.error),"success"in d&&n(6,f=d.success),"style"in d&&n(7,m=d.style),"$$scope"in d&&n(8,i=d.$$scope)},[r,a,o,l,c,u,f,m,i,s]}class Mi extends x{constructor(e){super(),$(this,e,Di,Oi,ee,{class:0,color:1,dense:2,readonly:3,disabled:4,error:5,success:6,style:7})}}let yn=36,hs="";for(;yn--;)hs+=yn.toString(36);const Pi=t=>{let e="",n=t||11;for(;n--;)e+=hs[Math.random()*36|0];return e},Ni="M12,2C17.53,2 22,6.47 22,12C22,17.53 17.53,22 12,22C6.47,22 2,17.53 2,12C2,6.47 6.47,2 12,2M15.59,7L12,10.59L8.41,7L7,8.41L10.59,12L7,15.59L8.41,17L12,13.41L15.59,17L17,15.59L13.41,12L17,8.41L15.59,7Z",Ui=t=>({}),bn=t=>({}),Vi=t=>({}),In=t=>({}),Hi=t=>({}),En=t=>({}),Bi=t=>({}),wn=t=>({}),Fi=t=>({}),Tn=t=>({slot:"prepend-outer"});function kn(t,e,n){const s=t.slice();return s[44]=e[n],s}function Cn(t,e,n){const s=t.slice();return s[44]=e[n],s}const Wi=t=>({}),Sn=t=>({slot:"append-outer"});function An(t){let e,n,s,i;const r=t[33]["clear-icon"],a=B(r,t,t[43],In),o=a||ji();return{c(){e=U("div"),o&&o.c(),this.h()},l(l){e=V(l,"DIV",{style:!0});var c=P(e);o&&o.l(c),c.forEach(S),this.h()},h(){Dt(e,"cursor","pointer")},m(l,c){G(l,e,c),o&&o.m(e,null),n=!0,s||(i=z(e,"click",t[26]),s=!0)},p(l,c){a&&a.p&&(!n||c[1]&4096)&&F(a,r,l,l[43],n?j(r,l[43],c,Vi):W(l[43]),In)},i(l){n||(M(o,l),n=!0)},o(l){N(o,l),n=!1},d(l){l&&S(e),o&&o.d(l),s=!1,i()}}}function ji(t){let e,n;return e=new Ei({props:{path:Ni}}),{c(){Wt(e.$$.fragment)},l(s){jt(e.$$.fragment,s)},m(s,i){zt(e,s,i),n=!0},p:Mt,i(s){n||(M(e.$$.fragment,s),n=!0)},o(s){N(e.$$.fragment,s),n=!1},d(s){Gt(e,s)}}}function zi(t){let e,n,s,i,r,a,o,l,c,u,f,m;const d=t[33].prepend,h=B(d,t,t[43],wn),y=t[33].default,g=B(y,t,t[43],null),p=t[33].content,A=B(p,t,t[43],En);let k=[{type:"text"},{placeholder:t[14]},{id:t[20]},{readOnly:t[12]},{disabled:t[13]},t[28]],D={};for(let _=0;_<k.length;_+=1)D=be(D,k[_]);let v=t[11]&&t[0]!==""&&An(t);const L=t[33].append,w=B(L,t,t[43],bn);return{c(){e=U("div"),h&&h.c(),n=q(),s=U("div"),i=U("label"),g&&g.c(),r=q(),A&&A.c(),a=q(),o=U("input"),l=q(),v&&v.c(),c=q(),w&&w.c(),this.h()},l(_){e=V(_,"DIV",{class:!0});var E=P(e);h&&h.l(E),n=K(E),s=V(E,"DIV",{class:!0});var C=P(s);i=V(C,"LABEL",{for:!0});var Y=P(i);g&&g.l(Y),Y.forEach(S),r=K(C),A&&A.l(C),a=K(C),o=V(C,"INPUT",{type:!0,placeholder:!0,id:!0}),C.forEach(S),l=K(E),v&&v.l(E),c=K(E),w&&w.l(E),E.forEach(S),this.h()},h(){I(i,"for",t[20]),b(i,"active",t[23]),at(o,D),I(s,"class","s-text-field__input"),I(e,"class","s-text-field__wrapper"),b(e,"filled",t[5]),b(e,"solo",t[6]),b(e,"outlined",t[7]),b(e,"flat",t[8]),b(e,"rounded",t[10])},m(_,E){G(_,e,E),h&&h.m(e,null),O(e,n),O(e,s),O(s,i),g&&g.m(i,null),O(s,r),A&&A.m(s,null),O(s,a),O(s,o),o.autofocus&&o.focus(),t[41](o),on(o,t[0]),O(e,l),v&&v.m(e,null),O(e,c),w&&w.m(e,null),u=!0,f||(m=[z(o,"input",t[42]),z(o,"focus",t[24]),z(o,"blur",t[25]),z(o,"input",t[27]),z(o,"focus",t[34]),z(o,"blur",t[35]),z(o,"input",t[36]),z(o,"change",t[37]),z(o,"keypress",t[38]),z(o,"keydown",t[39]),z(o,"keyup",t[40])],f=!0)},p(_,E){h&&h.p&&(!u||E[1]&4096)&&F(h,d,_,_[43],u?j(d,_[43],E,Bi):W(_[43]),wn),g&&g.p&&(!u||E[1]&4096)&&F(g,y,_,_[43],u?j(y,_[43],E,null):W(_[43]),null),(!u||E[0]&1048576)&&I(i,"for",_[20]),(!u||E[0]&8388608)&&b(i,"active",_[23]),A&&A.p&&(!u||E[1]&4096)&&F(A,p,_,_[43],u?j(p,_[43],E,Hi):W(_[43]),En),at(o,D=Ft(k,[{type:"text"},(!u||E[0]&16384)&&{placeholder:_[14]},(!u||E[0]&1048576)&&{id:_[20]},(!u||E[0]&4096)&&{readOnly:_[12]},(!u||E[0]&8192)&&{disabled:_[13]},E[0]&268435456&&_[28]])),E[0]&1&&o.value!==_[0]&&on(o,_[0]),_[11]&&_[0]!==""?v?(v.p(_,E),E[0]&2049&&M(v,1)):(v=An(_),v.c(),M(v,1),v.m(e,c)):v&&(qt(),N(v,1,1,()=>{v=null}),Kt()),w&&w.p&&(!u||E[1]&4096)&&F(w,L,_,_[43],u?j(L,_[43],E,Ui):W(_[43]),bn),(!u||E[0]&32)&&b(e,"filled",_[5]),(!u||E[0]&64)&&b(e,"solo",_[6]),(!u||E[0]&128)&&b(e,"outlined",_[7]),(!u||E[0]&256)&&b(e,"flat",_[8]),(!u||E[0]&1024)&&b(e,"rounded",_[10])},i(_){u||(M(h,_),M(g,_),M(A,_),M(v),M(w,_),u=!0)},o(_){N(h,_),N(g,_),N(A,_),N(v),N(w,_),u=!1},d(_){_&&S(e),h&&h.d(_),g&&g.d(_),A&&A.d(_),t[41](null),v&&v.d(),w&&w.d(_),f=!1,ze(m)}}}function Gi(t){let e;const n=t[33]["prepend-outer"],s=B(n,t,t[43],Tn);return{c(){s&&s.c()},l(i){s&&s.l(i)},m(i,r){s&&s.m(i,r),e=!0},p(i,r){s&&s.p&&(!e||r[1]&4096)&&F(s,n,i,i[43],e?j(n,i[43],r,Fi):W(i[43]),Tn)},i(i){e||(M(s,i),e=!0)},o(i){N(s,i),e=!1},d(i){s&&s.d(i)}}}function Ln(t){let e,n=t[44]+"",s;return{c(){e=U("span"),s=pe(n)},l(i){e=V(i,"SPAN",{});var r=P(e);s=ve(r,n),r.forEach(S)},m(i,r){G(i,e,r),O(e,s)},p(i,r){r[0]&131072&&n!==(n=i[44]+"")&&Ae(s,n)},d(i){i&&S(e)}}}function Rn(t){let e,n=t[44]+"",s;return{c(){e=U("span"),s=pe(n)},l(i){e=V(i,"SPAN",{});var r=P(e);s=ve(r,n),r.forEach(S)},m(i,r){G(i,e,r),O(e,s)},p(i,r){r[0]&4456448&&n!==(n=i[44]+"")&&Ae(s,n)},d(i){i&&S(e)}}}function On(t){let e,n=t[0].length+"",s,i,r;return{c(){e=U("span"),s=pe(n),i=pe(" / "),r=pe(t[16])},l(a){e=V(a,"SPAN",{});var o=P(e);s=ve(o,n),i=ve(o," / "),r=ve(o,t[16]),o.forEach(S)},m(a,o){G(a,e,o),O(e,s),O(e,i),O(e,r)},p(a,o){o[0]&1&&n!==(n=a[0].length+"")&&Ae(s,n),o[0]&65536&&Ae(r,a[16])},d(a){a&&S(e)}}}function qi(t){let e,n,s,i,r,a,o,l=t[17],c=[];for(let d=0;d<l.length;d+=1)c[d]=Ln(Cn(t,l,d));let u=t[22].slice(0,t[18]),f=[];for(let d=0;d<u.length;d+=1)f[d]=Rn(kn(t,u,d));let m=t[16]&&On(t);return{c(){e=U("div"),n=U("div"),s=U("span"),i=pe(t[15]),r=q();for(let d=0;d<c.length;d+=1)c[d].c();a=q();for(let d=0;d<f.length;d+=1)f[d].c();o=q(),m&&m.c(),this.h()},l(d){e=V(d,"DIV",{slot:!0});var h=P(e);n=V(h,"DIV",{});var y=P(n);s=V(y,"SPAN",{});var g=P(s);i=ve(g,t[15]),g.forEach(S),r=K(y);for(let p=0;p<c.length;p+=1)c[p].l(y);a=K(y);for(let p=0;p<f.length;p+=1)f[p].l(y);y.forEach(S),o=K(h),m&&m.l(h),h.forEach(S),this.h()},h(){I(e,"slot","messages")},m(d,h){G(d,e,h),O(e,n),O(n,s),O(s,i),O(n,r);for(let y=0;y<c.length;y+=1)c[y].m(n,null);O(n,a);for(let y=0;y<f.length;y+=1)f[y].m(n,null);O(e,o),m&&m.m(e,null)},p(d,h){if(h[0]&32768&&Ae(i,d[15]),h[0]&131072){l=d[17];let y;for(y=0;y<l.length;y+=1){const g=Cn(d,l,y);c[y]?c[y].p(g,h):(c[y]=Ln(g),c[y].c(),c[y].m(n,a))}for(;y<c.length;y+=1)c[y].d(1);c.length=l.length}if(h[0]&4456448){u=d[22].slice(0,d[18]);let y;for(y=0;y<u.length;y+=1){const g=kn(d,u,y);f[y]?f[y].p(g,h):(f[y]=Rn(g),f[y].c(),f[y].m(n,null))}for(;y<f.length;y+=1)f[y].d(1);f.length=u.length}d[16]?m?m.p(d,h):(m=On(d),m.c(),m.m(e,null)):m&&(m.d(1),m=null)},d(d){d&&S(e),ln(c,d),ln(f,d),m&&m.d()}}}function Ki(t){let e;const n=t[33]["append-outer"],s=B(n,t,t[43],Sn);return{c(){s&&s.c()},l(i){s&&s.l(i)},m(i,r){s&&s.m(i,r),e=!0},p(i,r){s&&s.p&&(!e||r[1]&4096)&&F(s,n,i,i[43],e?j(n,i[43],r,Wi):W(i[43]),Sn)},i(i){e||(M(s,i),e=!0)},o(i){N(s,i),e=!1},d(i){s&&s.d(i)}}}function Ji(t){let e,n;return e=new Mi({props:{class:"s-text-field "+t[3],color:t[4],dense:t[9],readonly:t[12],disabled:t[13],error:t[1],success:t[19],style:t[21],$$slots:{"append-outer":[Ki],messages:[qi],"prepend-outer":[Gi],default:[zi]},$$scope:{ctx:t}}}),{c(){Wt(e.$$.fragment)},l(s){jt(e.$$.fragment,s)},m(s,i){zt(e,s,i),n=!0},p(s,i){const r={};i[0]&8&&(r.class="s-text-field "+s[3]),i[0]&16&&(r.color=s[4]),i[0]&512&&(r.dense=s[9]),i[0]&4096&&(r.readonly=s[12]),i[0]&8192&&(r.disabled=s[13]),i[0]&2&&(r.error=s[1]),i[0]&524288&&(r.success=s[19]),i[0]&2097152&&(r.style=s[21]),i[0]&282590693|i[1]&4096&&(r.$$scope={dirty:i,ctx:s}),e.$set(r)},i(s){n||(M(e.$$.fragment,s),n=!0)},o(s){N(e.$$.fragment,s),n=!1},d(s){Gt(e,s)}}}function Xi(t,e,n){let s;const i=["class","value","color","filled","solo","outlined","flat","dense","rounded","clearable","readonly","disabled","placeholder","hint","counter","messages","rules","errorCount","validateOnBlur","error","success","id","style","inputElement","validate"];let r=ot(e,i),{$$slots:a={},$$scope:o}=e,{class:l=""}=e,{value:c=""}=e,{color:u="primary"}=e,{filled:f=!1}=e,{solo:m=!1}=e,{outlined:d=!1}=e,{flat:h=!1}=e,{dense:y=!1}=e,{rounded:g=!1}=e,{clearable:p=!1}=e,{readonly:A=!1}=e,{disabled:k=!1}=e,{placeholder:D=null}=e,{hint:v=""}=e,{counter:L=!1}=e,{messages:w=[]}=e,{rules:_=[]}=e,{errorCount:E=1}=e,{validateOnBlur:C=!1}=e,{error:Y=!1}=e,{success:De=!1}=e,{id:rn=`s-input-${Pi(5)}`}=e,{style:an=null}=e,{inputElement:$e=null}=e,et=!1,yt=[];function bt(){return n(22,yt=_.map(T=>T(c)).filter(T=>typeof T=="string")),yt.length?n(1,Y=!0):n(1,Y=!1),Y}function ei(){n(32,et=!0)}function ti(){n(32,et=!1),C&&bt()}function ni(){n(0,c="")}function si(){C||bt()}function ii(T){X.call(this,t,T)}function ri(T){X.call(this,t,T)}function ai(T){X.call(this,t,T)}function oi(T){X.call(this,t,T)}function li(T){X.call(this,t,T)}function ci(T){X.call(this,t,T)}function ui(T){X.call(this,t,T)}function di(T){cs[T?"unshift":"push"](()=>{$e=T,n(2,$e)})}function fi(){c=this.value,n(0,c)}return t.$$set=T=>{e=be(be({},e),ls(T)),n(28,r=ot(e,i)),"class"in T&&n(3,l=T.class),"value"in T&&n(0,c=T.value),"color"in T&&n(4,u=T.color),"filled"in T&&n(5,f=T.filled),"solo"in T&&n(6,m=T.solo),"outlined"in T&&n(7,d=T.outlined),"flat"in T&&n(8,h=T.flat),"dense"in T&&n(9,y=T.dense),"rounded"in T&&n(10,g=T.rounded),"clearable"in T&&n(11,p=T.clearable),"readonly"in T&&n(12,A=T.readonly),"disabled"in T&&n(13,k=T.disabled),"placeholder"in T&&n(14,D=T.placeholder),"hint"in T&&n(15,v=T.hint),"counter"in T&&n(16,L=T.counter),"messages"in T&&n(17,w=T.messages),"rules"in T&&n(29,_=T.rules),"errorCount"in T&&n(18,E=T.errorCount),"validateOnBlur"in T&&n(30,C=T.validateOnBlur),"error"in T&&n(1,Y=T.error),"success"in T&&n(19,De=T.success),"id"in T&&n(20,rn=T.id),"style"in T&&n(21,an=T.style),"inputElement"in T&&n(2,$e=T.inputElement),"$$scope"in T&&n(43,o=T.$$scope)},t.$$.update=()=>{t.$$.dirty[0]&16385|t.$$.dirty[1]&2&&n(23,s=!!D||c||et)},[c,Y,$e,l,u,f,m,d,h,y,g,p,A,k,D,v,L,w,E,De,rn,an,yt,s,ei,ti,ni,si,r,_,C,bt,et,a,ii,ri,ai,oi,li,ci,ui,di,fi,o]}class cc extends x{constructor(e){super(),$(this,e,Xi,Ji,ee,{class:3,value:0,color:4,filled:5,solo:6,outlined:7,flat:8,dense:9,rounded:10,clearable:11,readonly:12,disabled:13,placeholder:14,hint:15,counter:16,messages:17,rules:29,errorCount:18,validateOnBlur:30,error:1,success:19,id:20,style:21,inputElement:2,validate:31},null,[-1,-1])}get validate(){return this.$$.ctx[31]}}function Zi(t){const e=t-1;return e*e*e+1}function Yi(t,{delay:e=0,duration:n=400,easing:s=hi}={}){const i=+getComputedStyle(t).opacity;return{delay:e,duration:n,easing:s,css:r=>`opacity: ${r*i}`}}function Qi(t,{delay:e=0,duration:n=400,easing:s=Zi,start:i=0,opacity:r=0}={}){const a=getComputedStyle(t),o=+a.opacity,l=a.transform==="none"?"":a.transform,c=1-i,u=o*(1-r);return{delay:e,duration:n,easing:s,css:(f,m)=>`
			transform: ${l} scale(${1-c*m});
			opacity: ${o-u*m}
		`}}function xi(t){let e,n,s;const i=t[10].default,r=B(i,t,t[9],null);return{c(){e=U("div"),r&&r.c(),this.h()},l(a){e=V(a,"DIV",{role:!0,class:!0,"aria-disabled":!0,style:!0});var o=P(e);r&&r.l(o),o.forEach(S),this.h()},h(){I(e,"role",t[8]),I(e,"class",n="s-list "+t[0]),I(e,"aria-disabled",t[2]),I(e,"style",t[7]),b(e,"dense",t[1]),b(e,"disabled",t[2]),b(e,"flat",t[3]),b(e,"nav",t[5]),b(e,"outlined",t[6]),b(e,"rounded",t[4])},m(a,o){G(a,e,o),r&&r.m(e,null),s=!0},p(a,[o]){r&&r.p&&(!s||o&512)&&F(r,i,a,a[9],s?j(i,a[9],o,null):W(a[9]),null),(!s||o&256)&&I(e,"role",a[8]),(!s||o&1&&n!==(n="s-list "+a[0]))&&I(e,"class",n),(!s||o&4)&&I(e,"aria-disabled",a[2]),(!s||o&128)&&I(e,"style",a[7]),(!s||o&3)&&b(e,"dense",a[1]),(!s||o&5)&&b(e,"disabled",a[2]),(!s||o&9)&&b(e,"flat",a[3]),(!s||o&33)&&b(e,"nav",a[5]),(!s||o&65)&&b(e,"outlined",a[6]),(!s||o&17)&&b(e,"rounded",a[4])},i(a){s||(M(r,a),s=!0)},o(a){N(r,a),s=!1},d(a){a&&S(e),r&&r.d(a)}}}function $i(t,e,n){let{$$slots:s={},$$scope:i}=e,{class:r=""}=e,{dense:a=null}=e,{disabled:o=null}=e,{flat:l=!1}=e,{rounded:c=!1}=e,{nav:u=!1}=e,{outlined:f=!1}=e,{style:m=null}=e,d=null;return Me("S_ListItemRole")||(_i("S_ListItemRole","listitem"),d="list"),t.$$set=h=>{"class"in h&&n(0,r=h.class),"dense"in h&&n(1,a=h.dense),"disabled"in h&&n(2,o=h.disabled),"flat"in h&&n(3,l=h.flat),"rounded"in h&&n(4,c=h.rounded),"nav"in h&&n(5,u=h.nav),"outlined"in h&&n(6,f=h.outlined),"style"in h&&n(7,m=h.style),"$$scope"in h&&n(9,i=h.$$scope)},[r,a,o,l,c,u,f,m,d,i,s]}class uc extends x{constructor(e){super(),$(this,e,$i,xi,ee,{class:0,dense:1,disabled:2,flat:3,rounded:4,nav:5,outlined:6,style:7})}}const er=t=>({}),Dn=t=>({}),tr=t=>({}),Mn=t=>({}),nr=t=>({}),Pn=t=>({});function sr(t){let e,n,s,i,r,a,o,l,c,u,f,m,d,h,y;const g=t[14].prepend,p=B(g,t,t[13],Pn),A=t[14].default,k=B(A,t,t[13],null),D=t[14].subtitle,v=B(D,t,t[13],Mn),L=t[14].append,w=B(L,t,t[13],Dn);return{c(){e=U("div"),p&&p.c(),n=q(),s=U("div"),i=U("div"),k&&k.c(),r=q(),a=U("div"),v&&v.c(),o=q(),w&&w.c(),this.h()},l(_){e=V(_,"DIV",{class:!0,role:!0,tabindex:!0,"aria-selected":!0,style:!0});var E=P(e);p&&p.l(E),n=K(E),s=V(E,"DIV",{class:!0});var C=P(s);i=V(C,"DIV",{class:!0});var Y=P(i);k&&k.l(Y),Y.forEach(S),r=K(C),a=V(C,"DIV",{class:!0});var De=P(a);v&&v.l(De),De.forEach(S),C.forEach(S),o=K(E),w&&w.l(E),E.forEach(S),this.h()},h(){I(i,"class","s-list-item__title"),I(a,"class","s-list-item__subtitle"),I(s,"class","s-list-item__content"),I(e,"class",l="s-list-item "+t[1]),I(e,"role",t[10]),I(e,"tabindex",c=t[6]?0:-1),I(e,"aria-selected",u=t[10]==="option"?t[0]:null),I(e,"style",t[9]),b(e,"dense",t[3]),b(e,"disabled",t[4]),b(e,"multiline",t[5]),b(e,"link",t[6]),b(e,"selectable",t[7])},m(_,E){G(_,e,E),p&&p.m(e,null),O(e,n),O(e,s),O(s,i),k&&k.m(i,null),O(s,r),O(s,a),v&&v.m(a,null),O(e,o),w&&w.m(e,null),d=!0,h||(y=[re(f=Jt.call(null,e,[t[0]&&t[2]])),re(m=ds.call(null,e,t[8])),z(e,"click",t[11]),z(e,"click",t[15]),z(e,"dblclick",t[16])],h=!0)},p(_,[E]){p&&p.p&&(!d||E&8192)&&F(p,g,_,_[13],d?j(g,_[13],E,nr):W(_[13]),Pn),k&&k.p&&(!d||E&8192)&&F(k,A,_,_[13],d?j(A,_[13],E,null):W(_[13]),null),v&&v.p&&(!d||E&8192)&&F(v,D,_,_[13],d?j(D,_[13],E,tr):W(_[13]),Mn),w&&w.p&&(!d||E&8192)&&F(w,L,_,_[13],d?j(L,_[13],E,er):W(_[13]),Dn),(!d||E&2&&l!==(l="s-list-item "+_[1]))&&I(e,"class",l),(!d||E&64&&c!==(c=_[6]?0:-1))&&I(e,"tabindex",c),(!d||E&1&&u!==(u=_[10]==="option"?_[0]:null))&&I(e,"aria-selected",u),(!d||E&512)&&I(e,"style",_[9]),f&&ae(f.update)&&E&5&&f.update.call(null,[_[0]&&_[2]]),m&&ae(m.update)&&E&256&&m.update.call(null,_[8]),(!d||E&10)&&b(e,"dense",_[3]),(!d||E&18)&&b(e,"disabled",_[4]),(!d||E&34)&&b(e,"multiline",_[5]),(!d||E&66)&&b(e,"link",_[6]),(!d||E&130)&&b(e,"selectable",_[7])},i(_){d||(M(p,_),M(k,_),M(v,_),M(w,_),d=!0)},o(_){N(p,_),N(k,_),N(v,_),N(w,_),d=!1},d(_){_&&S(e),p&&p.d(_),k&&k.d(_),v&&v.d(_),w&&w.d(_),h=!1,ze(y)}}}function ir(t,e,n){let{$$slots:s={},$$scope:i}=e;const r=Me("S_ListItemRole"),a=Me("S_ListItemGroup"),l=a?Me(a):{select:()=>null,register:()=>null,index:()=>null,activeClass:"active"};let{class:c=""}=e,{activeClass:u=l.activeClass}=e,{value:f=l.index()}=e,{active:m=!1}=e,{dense:d=!1}=e,{disabled:h=null}=e,{multiline:y=!1}=e,{link:g=r}=e,{selectable:p=!g}=e,{ripple:A=Me("S_ListItemRipple")||r||!1}=e,{style:k=null}=e;l.register(w=>{n(0,m=w.includes(f))});function D(){h||l.select(f)}function v(w){X.call(this,t,w)}function L(w){X.call(this,t,w)}return t.$$set=w=>{"class"in w&&n(1,c=w.class),"activeClass"in w&&n(2,u=w.activeClass),"value"in w&&n(12,f=w.value),"active"in w&&n(0,m=w.active),"dense"in w&&n(3,d=w.dense),"disabled"in w&&n(4,h=w.disabled),"multiline"in w&&n(5,y=w.multiline),"link"in w&&n(6,g=w.link),"selectable"in w&&n(7,p=w.selectable),"ripple"in w&&n(8,A=w.ripple),"style"in w&&n(9,k=w.style),"$$scope"in w&&n(13,i=w.$$scope)},[m,c,u,d,h,y,g,p,A,k,r,D,f,i,s,v,L]}class dc extends x{constructor(e){super(),$(this,e,ir,sr,ee,{class:1,activeClass:2,value:12,active:0,dense:3,disabled:4,multiline:5,link:6,selectable:7,ripple:8,style:9})}}const rr=["primary","secondary","success","info","warning","error"];function ar(t){return t.split(" ").map(e=>rr.includes(e)?`${e}-color`:e)}function Nn(t,e){if(/^(#|rgb|hsl|currentColor)/.test(e))return t.style.backgroundColor=e,!1;if(e.startsWith("--"))return t.style.backgroundColor=`var(${e})`,!1;const n=ar(e);return t.classList.add(...n),n}const or=(t,e)=>{let n;return typeof e=="string"&&(n=Nn(t,e)),{update(s){n?t.classList.remove(...n):t.style.backgroundColor=null,typeof s=="string"&&(n=Nn(t,s))}}};function Un(t){let e,n,s,i,r,a,o,l,c,u,f,m;const d=t[11].default,h=B(d,t,t[10],null);return{c(){e=U("div"),n=U("div"),i=q(),r=U("div"),h&&h.c(),this.h()},l(y){e=V(y,"DIV",{class:!0,style:!0});var g=P(e);n=V(g,"DIV",{class:!0,style:!0}),P(n).forEach(S),i=K(g),r=V(g,"DIV",{class:!0});var p=P(r);h&&h.l(p),p.forEach(S),g.forEach(S),this.h()},h(){I(n,"class","s-overlay__scrim svelte-zop6hb"),Dt(n,"opacity",t[5]),I(r,"class","s-overlay__content svelte-zop6hb"),I(e,"class",a="s-overlay "+t[0]+" svelte-zop6hb"),I(e,"style",o="z-index:"+t[7]+";"+t[9]),b(e,"absolute",t[8])},m(y,g){G(y,e,g),O(e,n),O(e,i),O(e,r),h&&h.m(r,null),u=!0,f||(m=[re(s=or.call(null,n,t[6])),z(e,"click",t[12])],f=!0)},p(y,g){t=y,(!u||g&32)&&Dt(n,"opacity",t[5]),s&&ae(s.update)&&g&64&&s.update.call(null,t[6]),h&&h.p&&(!u||g&1024)&&F(h,d,t,t[10],u?j(d,t[10],g,null):W(t[10]),null),(!u||g&1&&a!==(a="s-overlay "+t[0]+" svelte-zop6hb"))&&I(e,"class",a),(!u||g&640&&o!==(o="z-index:"+t[7]+";"+t[9]))&&I(e,"style",o),(!u||g&257)&&b(e,"absolute",t[8])},i(y){u||(M(h,y),us(()=>{c&&c.end(1),l=mi(e,t[1],t[2]),l.start()}),u=!0)},o(y){N(h,y),l&&l.invalidate(),c=gi(e,t[1],t[3]),u=!1},d(y){y&&S(e),h&&h.d(y),y&&c&&c.end(),f=!1,ze(m)}}}function lr(t){let e,n,s=t[4]&&Un(t);return{c(){s&&s.c(),e=cn()},l(i){s&&s.l(i),e=cn()},m(i,r){s&&s.m(i,r),G(i,e,r),n=!0},p(i,[r]){i[4]?s?(s.p(i,r),r&16&&M(s,1)):(s=Un(i),s.c(),M(s,1),s.m(e.parentNode,e)):s&&(qt(),N(s,1,1,()=>{s=null}),Kt())},i(i){n||(M(s),n=!0)},o(i){N(s),n=!1},d(i){s&&s.d(i),i&&S(e)}}}function cr(t,e,n){let{$$slots:s={},$$scope:i}=e,{class:r=""}=e,{transition:a=Yi}=e,{inOpts:o={duration:250}}=e,{outOpts:l={duration:250}}=e,{active:c=!0}=e,{opacity:u=.46}=e,{color:f="rgb(33, 33, 33)"}=e,{index:m=5}=e,{absolute:d=!1}=e,{style:h=""}=e;function y(g){X.call(this,t,g)}return t.$$set=g=>{"class"in g&&n(0,r=g.class),"transition"in g&&n(1,a=g.transition),"inOpts"in g&&n(2,o=g.inOpts),"outOpts"in g&&n(3,l=g.outOpts),"active"in g&&n(4,c=g.active),"opacity"in g&&n(5,u=g.opacity),"color"in g&&n(6,f=g.color),"index"in g&&n(7,m=g.index),"absolute"in g&&n(8,d=g.absolute),"style"in g&&n(9,h=g.style),"$$scope"in g&&n(10,i=g.$$scope)},[r,a,o,l,c,u,f,m,d,h,i,s,y]}class ur extends x{constructor(e){super(),$(this,e,cr,lr,ee,{class:0,transition:1,inOpts:2,outOpts:3,active:4,opacity:5,color:6,index:7,absolute:8,style:9})}}function Vn(t){let e,n,s,i,r,a,o,l;const c=t[11].default,u=B(c,t,t[10],null);return{c(){e=U("div"),n=U("div"),u&&u.c(),this.h()},l(f){e=V(f,"DIV",{role:!0,class:!0});var m=P(e);n=V(m,"DIV",{class:!0});var d=P(n);u&&u.l(d),d.forEach(S),m.forEach(S),this.h()},h(){I(n,"class",s="s-dialog__content "+t[0]),b(n,"fullscreen",t[2]),I(e,"role","document"),I(e,"class","s-dialog")},m(f,m){G(f,e,m),O(e,n),u&&u.m(n,null),a=!0,o||(l=[z(n,"introstart",t[12]),z(n,"outrostart",t[13]),z(n,"introend",t[14]),z(n,"outroend",t[15]),re(r=fs.call(null,e,{"dialog-width":t[1]}))],o=!0)},p(f,m){u&&u.p&&(!a||m&1024)&&F(u,c,f,f[10],a?j(c,f[10],m,null):W(f[10]),null),(!a||m&1&&s!==(s="s-dialog__content "+f[0]))&&I(n,"class",s),(!a||m&5)&&b(n,"fullscreen",f[2]),r&&ae(r.update)&&m&2&&r.update.call(null,{"dialog-width":f[1]})},i(f){a||(M(u,f),us(()=>{i||(i=un(n,t[3],{duration:300,start:.1},!0)),i.run(1)}),a=!0)},o(f){N(u,f),i||(i=un(n,t[3],{duration:300,start:.1},!1)),i.run(0),a=!1},d(f){f&&S(e),u&&u.d(f),f&&i&&i.end(),o=!1,ze(l)}}}function dr(t){let e,n,s,i=t[5]&&Vn(t);const r=[t[4],{active:t[5]}];let a={};for(let o=0;o<r.length;o+=1)a=be(a,r[o]);return n=new ur({props:a}),n.$on("click",t[6]),{c(){i&&i.c(),e=q(),Wt(n.$$.fragment)},l(o){i&&i.l(o),e=K(o),jt(n.$$.fragment,o)},m(o,l){i&&i.m(o,l),G(o,e,l),zt(n,o,l),s=!0},p(o,[l]){o[5]?i?(i.p(o,l),l&32&&M(i,1)):(i=Vn(o),i.c(),M(i,1),i.m(e.parentNode,e)):i&&(qt(),N(i,1,1,()=>{i=null}),Kt());const c=l&48?Ft(r,[l&16&&pi(o[4]),l&32&&{active:o[5]}]):{};n.$set(c)},i(o){s||(M(i),M(n.$$.fragment,o),s=!0)},o(o){N(i),N(n.$$.fragment,o),s=!1},d(o){i&&i.d(o),o&&S(e),Gt(n,o)}}}function fr(t,e,n){let s,{$$slots:i={},$$scope:r}=e,{class:a=""}=e,{active:o=!1}=e,{persistent:l=!1}=e,{disabled:c=!1}=e,{width:u=500}=e,{fullscreen:f=!1}=e,{transition:m=Qi}=e,{overlay:d={}}=e;function h(){l||n(7,o=!1)}function y(k){X.call(this,t,k)}function g(k){X.call(this,t,k)}function p(k){X.call(this,t,k)}function A(k){X.call(this,t,k)}return t.$$set=k=>{"class"in k&&n(0,a=k.class),"active"in k&&n(7,o=k.active),"persistent"in k&&n(8,l=k.persistent),"disabled"in k&&n(9,c=k.disabled),"width"in k&&n(1,u=k.width),"fullscreen"in k&&n(2,f=k.fullscreen),"transition"in k&&n(3,m=k.transition),"overlay"in k&&n(4,d=k.overlay),"$$scope"in k&&n(10,r=k.$$scope)},t.$$.update=()=>{t.$$.dirty&640&&n(5,s=o&&!c)},[a,u,f,m,d,s,h,o,l,c,r,i,y,g,p,A]}class fc extends x{constructor(e){super(),$(this,e,fr,dr,ee,{class:0,active:7,persistent:8,disabled:9,width:1,fullscreen:2,transition:3,overlay:4})}}function hr(t){let e,n,s;return{c(){e=U("hr"),this.h()},l(i){e=V(i,"HR",{class:!0,role:!0,"aria-orientation":!0,style:!0}),this.h()},h(){I(e,"class",n="s-divider "+t[0]+" svelte-wwsm4v"),I(e,"role","separator"),I(e,"aria-orientation",s=t[2]?"vertical":"horizontal"),I(e,"style",t[3]),b(e,"inset",t[1]),b(e,"vertical",t[2])},m(i,r){G(i,e,r)},p(i,[r]){r&1&&n!==(n="s-divider "+i[0]+" svelte-wwsm4v")&&I(e,"class",n),r&4&&s!==(s=i[2]?"vertical":"horizontal")&&I(e,"aria-orientation",s),r&8&&I(e,"style",i[3]),r&3&&b(e,"inset",i[1]),r&5&&b(e,"vertical",i[2])},i:Mt,o:Mt,d(i){i&&S(e)}}}function _r(t,e,n){let{class:s=""}=e,{inset:i=!1}=e,{vertical:r=!1}=e,{style:a=null}=e;return t.$$set=o=>{"class"in o&&n(0,s=o.class),"inset"in o&&n(1,i=o.inset),"vertical"in o&&n(2,r=o.vertical),"style"in o&&n(3,a=o.style)},[s,i,r,a]}class hc extends x{constructor(e){super(),$(this,e,_r,hr,ee,{class:0,inset:1,vertical:2,style:3})}}function mr(t){let e,n,s;const i=t[5].default,r=B(i,t,t[4],null);return{c(){e=U("div"),r&&r.c(),this.h()},l(a){e=V(a,"DIV",{class:!0,style:!0});var o=P(e);r&&r.l(o),o.forEach(S),this.h()},h(){I(e,"class",n="s-row "+t[0]),I(e,"style",t[3]),b(e,"dense",t[1]),b(e,"no-gutters",t[2])},m(a,o){G(a,e,o),r&&r.m(e,null),s=!0},p(a,[o]){r&&r.p&&(!s||o&16)&&F(r,i,a,a[4],s?j(i,a[4],o,null):W(a[4]),null),(!s||o&1&&n!==(n="s-row "+a[0]))&&I(e,"class",n),(!s||o&8)&&I(e,"style",a[3]),(!s||o&3)&&b(e,"dense",a[1]),(!s||o&5)&&b(e,"no-gutters",a[2])},i(a){s||(M(r,a),s=!0)},o(a){N(r,a),s=!1},d(a){a&&S(e),r&&r.d(a)}}}function gr(t,e,n){let{$$slots:s={},$$scope:i}=e,{class:r=""}=e,{dense:a=!1}=e,{noGutters:o=!1}=e,{style:l=null}=e;return t.$$set=c=>{"class"in c&&n(0,r=c.class),"dense"in c&&n(1,a=c.dense),"noGutters"in c&&n(2,o=c.noGutters),"style"in c&&n(3,l=c.style),"$$scope"in c&&n(4,i=c.$$scope)},[r,a,o,l,i,s]}class _c extends x{constructor(e){super(),$(this,e,gr,mr,ee,{class:0,dense:1,noGutters:2,style:3})}}function pr(t){let e,n,s,i,r,a;const o=t[13].default,l=B(o,t,t[12],null);return{c(){e=U("div"),l&&l.c(),this.h()},l(c){e=V(c,"DIV",{class:!0,style:!0});var u=P(e);l&&l.l(u),u.forEach(S),this.h()},h(){I(e,"class",n="s-col "+t[0]),I(e,"style",t[11])},m(c,u){G(c,e,u),l&&l.m(e,null),i=!0,r||(a=re(s=Jt.call(null,e,[t[1]&&`col-${t[1]}`,t[2]&&`sm-${t[2]}`,t[3]&&`md-${t[3]}`,t[4]&&`lg-${t[4]}`,t[5]&&`xl-${t[5]}`,t[6]&&`offset-${t[6]}`,t[7]&&`offset-sm-${t[7]}`,t[8]&&`offset-md-${t[8]}`,t[9]&&`offset-lg-${t[9]}`,t[10]&&`offset-xl-${t[10]}`])),r=!0)},p(c,[u]){l&&l.p&&(!i||u&4096)&&F(l,o,c,c[12],i?j(o,c[12],u,null):W(c[12]),null),(!i||u&1&&n!==(n="s-col "+c[0]))&&I(e,"class",n),(!i||u&2048)&&I(e,"style",c[11]),s&&ae(s.update)&&u&2046&&s.update.call(null,[c[1]&&`col-${c[1]}`,c[2]&&`sm-${c[2]}`,c[3]&&`md-${c[3]}`,c[4]&&`lg-${c[4]}`,c[5]&&`xl-${c[5]}`,c[6]&&`offset-${c[6]}`,c[7]&&`offset-sm-${c[7]}`,c[8]&&`offset-md-${c[8]}`,c[9]&&`offset-lg-${c[9]}`,c[10]&&`offset-xl-${c[10]}`])},i(c){i||(M(l,c),i=!0)},o(c){N(l,c),i=!1},d(c){c&&S(e),l&&l.d(c),r=!1,a()}}}function vr(t,e,n){let{$$slots:s={},$$scope:i}=e,{class:r=""}=e,{cols:a=!1}=e,{sm:o=!1}=e,{md:l=!1}=e,{lg:c=!1}=e,{xl:u=!1}=e,{offset:f=!1}=e,{offset_sm:m=!1}=e,{offset_md:d=!1}=e,{offset_lg:h=!1}=e,{offset_xl:y=!1}=e,{style:g=null}=e;return t.$$set=p=>{"class"in p&&n(0,r=p.class),"cols"in p&&n(1,a=p.cols),"sm"in p&&n(2,o=p.sm),"md"in p&&n(3,l=p.md),"lg"in p&&n(4,c=p.lg),"xl"in p&&n(5,u=p.xl),"offset"in p&&n(6,f=p.offset),"offset_sm"in p&&n(7,m=p.offset_sm),"offset_md"in p&&n(8,d=p.offset_md),"offset_lg"in p&&n(9,h=p.offset_lg),"offset_xl"in p&&n(10,y=p.offset_xl),"style"in p&&n(11,g=p.style),"$$scope"in p&&n(12,i=p.$$scope)},[r,a,o,l,c,u,f,m,d,h,y,g,i,s]}class mc extends x{constructor(e){super(),$(this,e,vr,pr,ee,{class:0,cols:1,sm:2,md:3,lg:4,xl:5,offset:6,offset_sm:7,offset_md:8,offset_lg:9,offset_xl:10,style:11})}}const gc=Z(""),pc=Z(!1),vc=Z(!1),yc=Z(null),yr=Z(null),bc=Z("pending"),Et=Z([]),Ic=Z(!1),Ec=Z(!1),wc=Z(!1),Tc=Z(!1),kc=Z(""),Cc=Z([]),Sc=Z({coin:"all",time:new Date().valueOf()}),Ac=Z({coin:"all",time:new Date().valueOf()});var Lc="M12,19.2C9.5,19.2 7.29,17.92 6,16C6.03,14 10,12.9 12,12.9C14,12.9 17.97,14 18,16C16.71,17.92 14.5,19.2 12,19.2M12,5A3,3 0 0,1 15,8A3,3 0 0,1 12,11A3,3 0 0,1 9,8A3,3 0 0,1 12,5M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12C22,6.47 17.5,2 12,2Z",Rc="M15,14C17.67,14 23,15.33 23,18V20H7V18C7,15.33 12.33,14 15,14M15,12A4,4 0 0,1 11,8A4,4 0 0,1 15,4A4,4 0 0,1 19,8A4,4 0 0,1 15,12M5,9.59L7.12,7.46L8.54,8.88L6.41,11L8.54,13.12L7.12,14.54L5,12.41L2.88,14.54L1.46,13.12L3.59,11L1.46,8.88L2.88,7.46L5,9.59Z",Oc="M12 2C17.5 2 22 6.5 22 12S17.5 22 12 22 2 17.5 2 12 6.5 2 12 2M12 4C10.1 4 8.4 4.6 7.1 5.7L18.3 16.9C19.3 15.5 20 13.8 20 12C20 7.6 16.4 4 12 4M16.9 18.3L5.7 7.1C4.6 8.4 4 10.1 4 12C4 16.4 7.6 20 12 20C13.9 20 15.6 19.4 16.9 18.3Z",Dc="M19,21H8V7H19M19,5H8A2,2 0 0,0 6,7V21A2,2 0 0,0 8,23H19A2,2 0 0,0 21,21V7A2,2 0 0,0 19,5M16,1H4A2,2 0 0,0 2,3V17H4V3H16V1Z",Mc="M17,7V3H7V7H17M14,17A3,3 0 0,0 17,14A3,3 0 0,0 14,11A3,3 0 0,0 11,14A3,3 0 0,0 14,17M19,1L23,5V17A2,2 0 0,1 21,19H7C5.89,19 5,18.1 5,17V3A2,2 0 0,1 7,1H19M1,7H3V21H17V23H3A2,2 0 0,1 1,21V7Z",Pc="M4,14V17C4,19 7.05,20.72 11,21V18.11L11.13,18C7.12,17.76 4,16.06 4,14M12,13C7.58,13 4,11.21 4,9V12C4,14.21 7.58,16 12,16C12.39,16 12.77,16 13.16,16L17,12.12C15.4,12.72 13.71,13 12,13M12,3C7.58,3 4,4.79 4,7C4,9.21 7.58,11 12,11C16.42,11 20,9.21 20,7C20,4.79 16.42,3 12,3M21,11.13C20.85,11.13 20.71,11.19 20.61,11.3L19.61,12.3L21.66,14.3L22.66,13.3C22.87,13.1 22.88,12.76 22.66,12.53L21.42,11.3C21.32,11.19 21.18,11.13 21.04,11.13M19.04,12.88L13,18.94V21H15.06L21.12,14.93L19.04,12.88Z",Nc="M12,16A2,2 0 0,1 14,18A2,2 0 0,1 12,20A2,2 0 0,1 10,18A2,2 0 0,1 12,16M12,10A2,2 0 0,1 14,12A2,2 0 0,1 12,14A2,2 0 0,1 10,12A2,2 0 0,1 12,10M12,4A2,2 0 0,1 14,6A2,2 0 0,1 12,8A2,2 0 0,1 10,6A2,2 0 0,1 12,4Z",Uc="M13 19C13 15.69 15.69 13 19 13C20.1 13 21.12 13.3 22 13.81V6C22 4.89 21.1 4 20 4H4C2.89 4 2 4.89 2 6V18C2 19.11 2.9 20 4 20H13.09C13.04 19.67 13 19.34 13 19M4 8V6L12 11L20 6V8L12 13L4 8M20 15V18H23V20H20V23H18V20H15V18H18V15H20Z",Vc="M6 2C4.9 2 4 2.9 4 4V20C4 21.1 4.9 22 6 22H10V20.1L20 10.1V8L14 2H6M13 3.5L18.5 9H13V3.5M20.1 13C20 13 19.8 13.1 19.7 13.2L18.7 14.2L20.8 16.3L21.8 15.3C22 15.1 22 14.7 21.8 14.5L20.5 13.2C20.4 13.1 20.3 13 20.1 13M18.1 14.8L12 20.9V23H14.1L20.2 16.9L18.1 14.8Z",Hc="M22 12L18 8V11H10V13H18V16M20 18A10 10 0 1 1 20 6H17.27A8 8 0 1 0 17.27 18Z",Bc="M10,17V14H3V10H10V7L15,12L10,17M10,2H19A2,2 0 0,1 21,4V20A2,2 0 0,1 19,22H10A2,2 0 0,1 8,20V18H10V20H19V4H10V6H8V4A2,2 0 0,1 10,2Z",Fc="M16,17V14H9V10H16V7L21,12L16,17M14,2A2,2 0 0,1 16,4V6H14V4H5V20H14V18H16V20A2,2 0 0,1 14,22H5A2,2 0 0,1 3,20V4A2,2 0 0,1 5,2H14Z",Wc="M17,13H7V11H17M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z",jc="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z",zc="M18.66,2C18.4,2 18.16,2.09 17.97,2.28L16.13,4.13L19.88,7.88L21.72,6.03C22.11,5.64 22.11,5 21.72,4.63L19.38,2.28C19.18,2.09 18.91,2 18.66,2M3.28,4L2,5.28L8.5,11.75L4,16.25V20H7.75L12.25,15.5L18.72,22L20,20.72L13.5,14.25L9.75,10.5L3.28,4M15.06,5.19L11.03,9.22L14.78,12.97L18.81,8.94L15.06,5.19Z",Gc="M17,13H13V17H11V13H7V11H11V7H13V11H17M19,3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3Z",qc="M17.65,6.35C16.2,4.9 14.21,4 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20C15.73,20 18.84,17.45 19.73,14H17.65C16.83,16.33 14.61,18 12,18A6,6 0 0,1 6,12A6,6 0 0,1 12,6C13.66,6 15.14,6.69 16.22,7.78L13,11H20V4L17.65,6.35Z",Kc="M12 2A10 10 0 1 0 22 12A10 10 0 0 0 12 2M18 11H13L14.81 9.19A3.94 3.94 0 0 0 12 8A4 4 0 1 0 15.86 13H17.91A6 6 0 1 1 12 6A5.91 5.91 0 0 1 16.22 7.78L18 6Z",Jc="M7.5,2C5.71,3.15 4.5,5.18 4.5,7.5C4.5,9.82 5.71,11.85 7.53,13C4.46,13 2,10.54 2,7.5A5.5,5.5 0 0,1 7.5,2M19.07,3.5L20.5,4.93L4.93,20.5L3.5,19.07L19.07,3.5M12.89,5.93L11.41,5L9.97,6L10.39,4.3L9,3.24L10.75,3.12L11.33,1.47L12,3.1L13.73,3.13L12.38,4.26L12.89,5.93M9.59,9.54L8.43,8.81L7.31,9.59L7.65,8.27L6.56,7.44L7.92,7.35L8.37,6.06L8.88,7.33L10.24,7.36L9.19,8.23L9.59,9.54M19,13.5A5.5,5.5 0 0,1 13.5,19C12.28,19 11.15,18.6 10.24,17.93L17.93,10.24C18.6,11.15 19,12.28 19,13.5M14.6,20.08L17.37,18.93L17.13,22.28L14.6,20.08M18.93,17.38L20.08,14.61L22.28,17.15L18.93,17.38M20.08,12.42L18.94,9.64L22.28,9.88L20.08,12.42M9.63,18.93L12.4,20.08L9.87,22.27L9.63,18.93Z",Xc="M21,18V19A2,2 0 0,1 19,21H5C3.89,21 3,20.1 3,19V5A2,2 0 0,1 5,3H19A2,2 0 0,1 21,5V6H12C10.89,6 10,6.9 10,8V16A2,2 0 0,0 12,18M12,16H22V8H12M16,13.5A1.5,1.5 0 0,1 14.5,12A1.5,1.5 0 0,1 16,10.5A1.5,1.5 0 0,1 17.5,12A1.5,1.5 0 0,1 16,13.5Z";const br="margin:auto;width:98%;max-width:640px",Ir="margin-left:1.5rem;margin-right:1.5rem",Er="margin-right:1.5rem",wr="margin-left:1.5rem",Tr="margin-left:1rem;margin-right:1rem",kr="margin-right:1rem",Cr="margin-left:1rem",Sr="margin-left:0.5rem;margin-right:0.5rem",Ar="margin-right:0.5rem",Lr="margin-left:0.5rem",Zc={pools:br,maBig:Ir,marBig:Er,malBig:wr,maMed:Tr,marMed:kr,malMed:Cr,maSml:Sr,marSml:Ar,malSml:Lr};/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *//**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const _s=function(t){const e=[];let n=0;for(let s=0;s<t.length;s++){let i=t.charCodeAt(s);i<128?e[n++]=i:i<2048?(e[n++]=i>>6|192,e[n++]=i&63|128):(i&64512)===55296&&s+1<t.length&&(t.charCodeAt(s+1)&64512)===56320?(i=65536+((i&1023)<<10)+(t.charCodeAt(++s)&1023),e[n++]=i>>18|240,e[n++]=i>>12&63|128,e[n++]=i>>6&63|128,e[n++]=i&63|128):(e[n++]=i>>12|224,e[n++]=i>>6&63|128,e[n++]=i&63|128)}return e},Rr=function(t){const e=[];let n=0,s=0;for(;n<t.length;){const i=t[n++];if(i<128)e[s++]=String.fromCharCode(i);else if(i>191&&i<224){const r=t[n++];e[s++]=String.fromCharCode((i&31)<<6|r&63)}else if(i>239&&i<365){const r=t[n++],a=t[n++],o=t[n++],l=((i&7)<<18|(r&63)<<12|(a&63)<<6|o&63)-65536;e[s++]=String.fromCharCode(55296+(l>>10)),e[s++]=String.fromCharCode(56320+(l&1023))}else{const r=t[n++],a=t[n++];e[s++]=String.fromCharCode((i&15)<<12|(r&63)<<6|a&63)}}return e.join("")},ms={byteToCharMap_:null,charToByteMap_:null,byteToCharMapWebSafe_:null,charToByteMapWebSafe_:null,ENCODED_VALS_BASE:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",get ENCODED_VALS(){return this.ENCODED_VALS_BASE+"+/="},get ENCODED_VALS_WEBSAFE(){return this.ENCODED_VALS_BASE+"-_."},HAS_NATIVE_SUPPORT:typeof atob=="function",encodeByteArray(t,e){if(!Array.isArray(t))throw Error("encodeByteArray takes an array as a parameter");this.init_();const n=e?this.byteToCharMapWebSafe_:this.byteToCharMap_,s=[];for(let i=0;i<t.length;i+=3){const r=t[i],a=i+1<t.length,o=a?t[i+1]:0,l=i+2<t.length,c=l?t[i+2]:0,u=r>>2,f=(r&3)<<4|o>>4;let m=(o&15)<<2|c>>6,d=c&63;l||(d=64,a||(m=64)),s.push(n[u],n[f],n[m],n[d])}return s.join("")},encodeString(t,e){return this.HAS_NATIVE_SUPPORT&&!e?btoa(t):this.encodeByteArray(_s(t),e)},decodeString(t,e){return this.HAS_NATIVE_SUPPORT&&!e?atob(t):Rr(this.decodeStringToByteArray(t,e))},decodeStringToByteArray(t,e){this.init_();const n=e?this.charToByteMapWebSafe_:this.charToByteMap_,s=[];for(let i=0;i<t.length;){const r=n[t.charAt(i++)],o=i<t.length?n[t.charAt(i)]:0;++i;const c=i<t.length?n[t.charAt(i)]:64;++i;const f=i<t.length?n[t.charAt(i)]:64;if(++i,r==null||o==null||c==null||f==null)throw Error();const m=r<<2|o>>4;if(s.push(m),c!==64){const d=o<<4&240|c>>2;if(s.push(d),f!==64){const h=c<<6&192|f;s.push(h)}}}return s},init_(){if(!this.byteToCharMap_){this.byteToCharMap_={},this.charToByteMap_={},this.byteToCharMapWebSafe_={},this.charToByteMapWebSafe_={};for(let t=0;t<this.ENCODED_VALS.length;t++)this.byteToCharMap_[t]=this.ENCODED_VALS.charAt(t),this.charToByteMap_[this.byteToCharMap_[t]]=t,this.byteToCharMapWebSafe_[t]=this.ENCODED_VALS_WEBSAFE.charAt(t),this.charToByteMapWebSafe_[this.byteToCharMapWebSafe_[t]]=t,t>=this.ENCODED_VALS_BASE.length&&(this.charToByteMap_[this.ENCODED_VALS_WEBSAFE.charAt(t)]=t,this.charToByteMapWebSafe_[this.ENCODED_VALS.charAt(t)]=t)}}},Or=function(t){const e=_s(t);return ms.encodeByteArray(e,!0)},gs=function(t){return Or(t).replace(/\./g,"")},Dr=function(t){try{return ms.decodeString(t,!0)}catch(e){console.error("base64Decode failed: ",e)}return null};/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Mr{constructor(){this.reject=()=>{},this.resolve=()=>{},this.promise=new Promise((e,n)=>{this.resolve=e,this.reject=n})}wrapCallback(e){return(n,s)=>{n?this.reject(n):this.resolve(s),typeof e=="function"&&(this.promise.catch(()=>{}),e.length===1?e(n):e(n,s))}}}/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function J(){return typeof navigator<"u"&&typeof navigator.userAgent=="string"?navigator.userAgent:""}function Pr(){return typeof window<"u"&&!!(window.cordova||window.phonegap||window.PhoneGap)&&/ios|iphone|ipod|ipad|android|blackberry|iemobile/i.test(J())}function Nr(){const t=typeof chrome=="object"?chrome.runtime:typeof browser=="object"?browser.runtime:void 0;return typeof t=="object"&&t.id!==void 0}function Ur(){return typeof navigator=="object"&&navigator.product==="ReactNative"}function Vr(){const t=J();return t.indexOf("MSIE ")>=0||t.indexOf("Trident/")>=0}function Hr(){return typeof indexedDB=="object"}function Br(){return new Promise((t,e)=>{try{let n=!0;const s="validate-browser-context-for-indexeddb-analytics-module",i=self.indexedDB.open(s);i.onsuccess=()=>{i.result.close(),n||self.indexedDB.deleteDatabase(s),t(!0)},i.onupgradeneeded=()=>{n=!1},i.onerror=()=>{var r;e(((r=i.error)===null||r===void 0?void 0:r.message)||"")}}catch(n){e(n)}})}/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Fr="FirebaseError";class me extends Error{constructor(e,n,s){super(n),this.code=e,this.customData=s,this.name=Fr,Object.setPrototypeOf(this,me.prototype),Error.captureStackTrace&&Error.captureStackTrace(this,Ge.prototype.create)}}class Ge{constructor(e,n,s){this.service=e,this.serviceName=n,this.errors=s}create(e,...n){const s=n[0]||{},i=`${this.service}/${e}`,r=this.errors[e],a=r?Wr(r,s):"Error",o=`${this.serviceName}: ${a} (${i}).`;return new me(i,o,s)}}function Wr(t,e){return t.replace(jr,(n,s)=>{const i=e[s];return i!=null?String(i):`<${s}?>`})}const jr=/\{\$([^}]+)}/g;function zr(t){for(const e in t)if(Object.prototype.hasOwnProperty.call(t,e))return!1;return!0}function lt(t,e){if(t===e)return!0;const n=Object.keys(t),s=Object.keys(e);for(const i of n){if(!s.includes(i))return!1;const r=t[i],a=e[i];if(Hn(r)&&Hn(a)){if(!lt(r,a))return!1}else if(r!==a)return!1}for(const i of s)if(!n.includes(i))return!1;return!0}function Hn(t){return t!==null&&typeof t=="object"}/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function qe(t){const e=[];for(const[n,s]of Object.entries(t))Array.isArray(s)?s.forEach(i=>{e.push(encodeURIComponent(n)+"="+encodeURIComponent(i))}):e.push(encodeURIComponent(n)+"="+encodeURIComponent(s));return e.length?"&"+e.join("&"):""}function Pe(t){const e={};return t.replace(/^\?/,"").split("&").forEach(s=>{if(s){const[i,r]=s.split("=");e[decodeURIComponent(i)]=decodeURIComponent(r)}}),e}function Ne(t){const e=t.indexOf("?");if(!e)return"";const n=t.indexOf("#",e);return t.substring(e,n>0?n:void 0)}function Gr(t,e){const n=new qr(t,e);return n.subscribe.bind(n)}class qr{constructor(e,n){this.observers=[],this.unsubscribes=[],this.observerCount=0,this.task=Promise.resolve(),this.finalized=!1,this.onNoObservers=n,this.task.then(()=>{e(this)}).catch(s=>{this.error(s)})}next(e){this.forEachObserver(n=>{n.next(e)})}error(e){this.forEachObserver(n=>{n.error(e)}),this.close(e)}complete(){this.forEachObserver(e=>{e.complete()}),this.close()}subscribe(e,n,s){let i;if(e===void 0&&n===void 0&&s===void 0)throw new Error("Missing Observer.");Kr(e,["next","error","complete"])?i=e:i={next:e,error:n,complete:s},i.next===void 0&&(i.next=wt),i.error===void 0&&(i.error=wt),i.complete===void 0&&(i.complete=wt);const r=this.unsubscribeOne.bind(this,this.observers.length);return this.finalized&&this.task.then(()=>{try{this.finalError?i.error(this.finalError):i.complete()}catch{}}),this.observers.push(i),r}unsubscribeOne(e){this.observers===void 0||this.observers[e]===void 0||(delete this.observers[e],this.observerCount-=1,this.observerCount===0&&this.onNoObservers!==void 0&&this.onNoObservers(this))}forEachObserver(e){if(!this.finalized)for(let n=0;n<this.observers.length;n++)this.sendOne(n,e)}sendOne(e,n){this.task.then(()=>{if(this.observers!==void 0&&this.observers[e]!==void 0)try{n(this.observers[e])}catch(s){typeof console<"u"&&console.error&&console.error(s)}})}close(e){this.finalized||(this.finalized=!0,e!==void 0&&(this.finalError=e),this.task.then(()=>{this.observers=void 0,this.onNoObservers=void 0}))}}function Kr(t,e){if(typeof t!="object"||t===null)return!1;for(const n of e)if(n in t&&typeof t[n]=="function")return!0;return!1}function wt(){}/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function le(t){return t&&t._delegate?t._delegate:t}class Le{constructor(e,n,s){this.name=e,this.instanceFactory=n,this.type=s,this.multipleInstances=!1,this.serviceProps={},this.instantiationMode="LAZY",this.onInstanceCreated=null}setInstantiationMode(e){return this.instantiationMode=e,this}setMultipleInstances(e){return this.multipleInstances=e,this}setServiceProps(e){return this.serviceProps=e,this}setInstanceCreatedCallback(e){return this.onInstanceCreated=e,this}}/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const ge="[DEFAULT]";/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Jr{constructor(e,n){this.name=e,this.container=n,this.component=null,this.instances=new Map,this.instancesDeferred=new Map,this.instancesOptions=new Map,this.onInitCallbacks=new Map}get(e){const n=this.normalizeInstanceIdentifier(e);if(!this.instancesDeferred.has(n)){const s=new Mr;if(this.instancesDeferred.set(n,s),this.isInitialized(n)||this.shouldAutoInitialize())try{const i=this.getOrInitializeService({instanceIdentifier:n});i&&s.resolve(i)}catch{}}return this.instancesDeferred.get(n).promise}getImmediate(e){var n;const s=this.normalizeInstanceIdentifier(e==null?void 0:e.identifier),i=(n=e==null?void 0:e.optional)!==null&&n!==void 0?n:!1;if(this.isInitialized(s)||this.shouldAutoInitialize())try{return this.getOrInitializeService({instanceIdentifier:s})}catch(r){if(i)return null;throw r}else{if(i)return null;throw Error(`Service ${this.name} is not available`)}}getComponent(){return this.component}setComponent(e){if(e.name!==this.name)throw Error(`Mismatching Component ${e.name} for Provider ${this.name}.`);if(this.component)throw Error(`Component for ${this.name} has already been provided`);if(this.component=e,!!this.shouldAutoInitialize()){if(Zr(e))try{this.getOrInitializeService({instanceIdentifier:ge})}catch{}for(const[n,s]of this.instancesDeferred.entries()){const i=this.normalizeInstanceIdentifier(n);try{const r=this.getOrInitializeService({instanceIdentifier:i});s.resolve(r)}catch{}}}}clearInstance(e=ge){this.instancesDeferred.delete(e),this.instancesOptions.delete(e),this.instances.delete(e)}async delete(){const e=Array.from(this.instances.values());await Promise.all([...e.filter(n=>"INTERNAL"in n).map(n=>n.INTERNAL.delete()),...e.filter(n=>"_delete"in n).map(n=>n._delete())])}isComponentSet(){return this.component!=null}isInitialized(e=ge){return this.instances.has(e)}getOptions(e=ge){return this.instancesOptions.get(e)||{}}initialize(e={}){const{options:n={}}=e,s=this.normalizeInstanceIdentifier(e.instanceIdentifier);if(this.isInitialized(s))throw Error(`${this.name}(${s}) has already been initialized`);if(!this.isComponentSet())throw Error(`Component ${this.name} has not been registered yet`);const i=this.getOrInitializeService({instanceIdentifier:s,options:n});for(const[r,a]of this.instancesDeferred.entries()){const o=this.normalizeInstanceIdentifier(r);s===o&&a.resolve(i)}return i}onInit(e,n){var s;const i=this.normalizeInstanceIdentifier(n),r=(s=this.onInitCallbacks.get(i))!==null&&s!==void 0?s:new Set;r.add(e),this.onInitCallbacks.set(i,r);const a=this.instances.get(i);return a&&e(a,i),()=>{r.delete(e)}}invokeOnInitCallbacks(e,n){const s=this.onInitCallbacks.get(n);if(!!s)for(const i of s)try{i(e,n)}catch{}}getOrInitializeService({instanceIdentifier:e,options:n={}}){let s=this.instances.get(e);if(!s&&this.component&&(s=this.component.instanceFactory(this.container,{instanceIdentifier:Xr(e),options:n}),this.instances.set(e,s),this.instancesOptions.set(e,n),this.invokeOnInitCallbacks(s,e),this.component.onInstanceCreated))try{this.component.onInstanceCreated(this.container,e,s)}catch{}return s||null}normalizeInstanceIdentifier(e=ge){return this.component?this.component.multipleInstances?e:ge:e}shouldAutoInitialize(){return!!this.component&&this.component.instantiationMode!=="EXPLICIT"}}function Xr(t){return t===ge?void 0:t}function Zr(t){return t.instantiationMode==="EAGER"}/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Yr{constructor(e){this.name=e,this.providers=new Map}addComponent(e){const n=this.getProvider(e.name);if(n.isComponentSet())throw new Error(`Component ${e.name} has already been registered with ${this.name}`);n.setComponent(e)}addOrOverwriteComponent(e){this.getProvider(e.name).isComponentSet()&&this.providers.delete(e.name),this.addComponent(e)}getProvider(e){if(this.providers.has(e))return this.providers.get(e);const n=new Jr(e,this);return this.providers.set(e,n),n}getProviders(){return Array.from(this.providers.values())}}/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */var H;(function(t){t[t.DEBUG=0]="DEBUG",t[t.VERBOSE=1]="VERBOSE",t[t.INFO=2]="INFO",t[t.WARN=3]="WARN",t[t.ERROR=4]="ERROR",t[t.SILENT=5]="SILENT"})(H||(H={}));const Qr={debug:H.DEBUG,verbose:H.VERBOSE,info:H.INFO,warn:H.WARN,error:H.ERROR,silent:H.SILENT},xr=H.INFO,$r={[H.DEBUG]:"log",[H.VERBOSE]:"log",[H.INFO]:"info",[H.WARN]:"warn",[H.ERROR]:"error"},ea=(t,e,...n)=>{if(e<t.logLevel)return;const s=new Date().toISOString(),i=$r[e];if(i)console[i](`[${s}]  ${t.name}:`,...n);else throw new Error(`Attempted to log a message with an invalid logType (value: ${e})`)};class ps{constructor(e){this.name=e,this._logLevel=xr,this._logHandler=ea,this._userLogHandler=null}get logLevel(){return this._logLevel}set logLevel(e){if(!(e in H))throw new TypeError(`Invalid value "${e}" assigned to \`logLevel\``);this._logLevel=e}setLogLevel(e){this._logLevel=typeof e=="string"?Qr[e]:e}get logHandler(){return this._logHandler}set logHandler(e){if(typeof e!="function")throw new TypeError("Value assigned to `logHandler` must be a function");this._logHandler=e}get userLogHandler(){return this._userLogHandler}set userLogHandler(e){this._userLogHandler=e}debug(...e){this._userLogHandler&&this._userLogHandler(this,H.DEBUG,...e),this._logHandler(this,H.DEBUG,...e)}log(...e){this._userLogHandler&&this._userLogHandler(this,H.VERBOSE,...e),this._logHandler(this,H.VERBOSE,...e)}info(...e){this._userLogHandler&&this._userLogHandler(this,H.INFO,...e),this._logHandler(this,H.INFO,...e)}warn(...e){this._userLogHandler&&this._userLogHandler(this,H.WARN,...e),this._logHandler(this,H.WARN,...e)}error(...e){this._userLogHandler&&this._userLogHandler(this,H.ERROR,...e),this._logHandler(this,H.ERROR,...e)}}const ta=(t,e)=>e.some(n=>t instanceof n);let Bn,Fn;function na(){return Bn||(Bn=[IDBDatabase,IDBObjectStore,IDBIndex,IDBCursor,IDBTransaction])}function sa(){return Fn||(Fn=[IDBCursor.prototype.advance,IDBCursor.prototype.continue,IDBCursor.prototype.continuePrimaryKey])}const vs=new WeakMap,Pt=new WeakMap,ys=new WeakMap,Tt=new WeakMap,Xt=new WeakMap;function ia(t){const e=new Promise((n,s)=>{const i=()=>{t.removeEventListener("success",r),t.removeEventListener("error",a)},r=()=>{n(_e(t.result)),i()},a=()=>{s(t.error),i()};t.addEventListener("success",r),t.addEventListener("error",a)});return e.then(n=>{n instanceof IDBCursor&&vs.set(n,t)}).catch(()=>{}),Xt.set(e,t),e}function ra(t){if(Pt.has(t))return;const e=new Promise((n,s)=>{const i=()=>{t.removeEventListener("complete",r),t.removeEventListener("error",a),t.removeEventListener("abort",a)},r=()=>{n(),i()},a=()=>{s(t.error||new DOMException("AbortError","AbortError")),i()};t.addEventListener("complete",r),t.addEventListener("error",a),t.addEventListener("abort",a)});Pt.set(t,e)}let Nt={get(t,e,n){if(t instanceof IDBTransaction){if(e==="done")return Pt.get(t);if(e==="objectStoreNames")return t.objectStoreNames||ys.get(t);if(e==="store")return n.objectStoreNames[1]?void 0:n.objectStore(n.objectStoreNames[0])}return _e(t[e])},set(t,e,n){return t[e]=n,!0},has(t,e){return t instanceof IDBTransaction&&(e==="done"||e==="store")?!0:e in t}};function aa(t){Nt=t(Nt)}function oa(t){return t===IDBDatabase.prototype.transaction&&!("objectStoreNames"in IDBTransaction.prototype)?function(e,...n){const s=t.call(kt(this),e,...n);return ys.set(s,e.sort?e.sort():[e]),_e(s)}:sa().includes(t)?function(...e){return t.apply(kt(this),e),_e(vs.get(this))}:function(...e){return _e(t.apply(kt(this),e))}}function la(t){return typeof t=="function"?oa(t):(t instanceof IDBTransaction&&ra(t),ta(t,na())?new Proxy(t,Nt):t)}function _e(t){if(t instanceof IDBRequest)return ia(t);if(Tt.has(t))return Tt.get(t);const e=la(t);return e!==t&&(Tt.set(t,e),Xt.set(e,t)),e}const kt=t=>Xt.get(t);function ca(t,e,{blocked:n,upgrade:s,blocking:i,terminated:r}={}){const a=indexedDB.open(t,e),o=_e(a);return s&&a.addEventListener("upgradeneeded",l=>{s(_e(a.result),l.oldVersion,l.newVersion,_e(a.transaction))}),n&&a.addEventListener("blocked",()=>n()),o.then(l=>{r&&l.addEventListener("close",()=>r()),i&&l.addEventListener("versionchange",()=>i())}).catch(()=>{}),o}const ua=["get","getKey","getAll","getAllKeys","count"],da=["put","add","delete","clear"],Ct=new Map;function Wn(t,e){if(!(t instanceof IDBDatabase&&!(e in t)&&typeof e=="string"))return;if(Ct.get(e))return Ct.get(e);const n=e.replace(/FromIndex$/,""),s=e!==n,i=da.includes(n);if(!(n in(s?IDBIndex:IDBObjectStore).prototype)||!(i||ua.includes(n)))return;const r=async function(a,...o){const l=this.transaction(a,i?"readwrite":"readonly");let c=l.store;return s&&(c=c.index(o.shift())),(await Promise.all([c[n](...o),i&&l.done]))[0]};return Ct.set(e,r),r}aa(t=>({...t,get:(e,n,s)=>Wn(e,n)||t.get(e,n,s),has:(e,n)=>!!Wn(e,n)||t.has(e,n)}));/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class fa{constructor(e){this.container=e}getPlatformInfoString(){return this.container.getProviders().map(n=>{if(ha(n)){const s=n.getImmediate();return`${s.library}/${s.version}`}else return null}).filter(n=>n).join(" ")}}function ha(t){const e=t.getComponent();return(e==null?void 0:e.type)==="VERSION"}const Ut="@firebase/app",jn="0.7.32";/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Ie=new ps("@firebase/app"),_a="@firebase/app-compat",ma="@firebase/analytics-compat",ga="@firebase/analytics",pa="@firebase/app-check-compat",va="@firebase/app-check",ya="@firebase/auth",ba="@firebase/auth-compat",Ia="@firebase/database",Ea="@firebase/database-compat",wa="@firebase/functions",Ta="@firebase/functions-compat",ka="@firebase/installations",Ca="@firebase/installations-compat",Sa="@firebase/messaging",Aa="@firebase/messaging-compat",La="@firebase/performance",Ra="@firebase/performance-compat",Oa="@firebase/remote-config",Da="@firebase/remote-config-compat",Ma="@firebase/storage",Pa="@firebase/storage-compat",Na="@firebase/firestore",Ua="@firebase/firestore-compat",Va="firebase",Ha="9.9.4";/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const bs="[DEFAULT]",Ba={[Ut]:"fire-core",[_a]:"fire-core-compat",[ga]:"fire-analytics",[ma]:"fire-analytics-compat",[va]:"fire-app-check",[pa]:"fire-app-check-compat",[ya]:"fire-auth",[ba]:"fire-auth-compat",[Ia]:"fire-rtdb",[Ea]:"fire-rtdb-compat",[wa]:"fire-fn",[Ta]:"fire-fn-compat",[ka]:"fire-iid",[Ca]:"fire-iid-compat",[Sa]:"fire-fcm",[Aa]:"fire-fcm-compat",[La]:"fire-perf",[Ra]:"fire-perf-compat",[Oa]:"fire-rc",[Da]:"fire-rc-compat",[Ma]:"fire-gcs",[Pa]:"fire-gcs-compat",[Na]:"fire-fst",[Ua]:"fire-fst-compat","fire-js":"fire-js",[Va]:"fire-js-all"};/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const ct=new Map,Vt=new Map;function Fa(t,e){try{t.container.addComponent(e)}catch(n){Ie.debug(`Component ${e.name} failed to register with FirebaseApp ${t.name}`,n)}}function He(t){const e=t.name;if(Vt.has(e))return Ie.debug(`There were multiple attempts to register component ${e}.`),!1;Vt.set(e,t);for(const n of ct.values())Fa(n,t);return!0}function Is(t,e){const n=t.container.getProvider("heartbeat").getImmediate({optional:!0});return n&&n.triggerHeartbeat(),t.container.getProvider(e)}/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Wa={["no-app"]:"No Firebase App '{$appName}' has been created - call Firebase App.initializeApp()",["bad-app-name"]:"Illegal App name: '{$appName}",["duplicate-app"]:"Firebase App named '{$appName}' already exists with different options or config",["app-deleted"]:"Firebase App named '{$appName}' already deleted",["invalid-app-argument"]:"firebase.{$appName}() takes either no argument or a Firebase App instance.",["invalid-log-argument"]:"First argument to `onLog` must be null or a function.",["idb-open"]:"Error thrown when opening IndexedDB. Original error: {$originalErrorMessage}.",["idb-get"]:"Error thrown when reading from IndexedDB. Original error: {$originalErrorMessage}.",["idb-set"]:"Error thrown when writing to IndexedDB. Original error: {$originalErrorMessage}.",["idb-delete"]:"Error thrown when deleting from IndexedDB. Original error: {$originalErrorMessage}."},Ee=new Ge("app","Firebase",Wa);/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class ja{constructor(e,n,s){this._isDeleted=!1,this._options=Object.assign({},e),this._config=Object.assign({},n),this._name=n.name,this._automaticDataCollectionEnabled=n.automaticDataCollectionEnabled,this._container=s,this.container.addComponent(new Le("app",()=>this,"PUBLIC"))}get automaticDataCollectionEnabled(){return this.checkDestroyed(),this._automaticDataCollectionEnabled}set automaticDataCollectionEnabled(e){this.checkDestroyed(),this._automaticDataCollectionEnabled=e}get name(){return this.checkDestroyed(),this._name}get options(){return this.checkDestroyed(),this._options}get config(){return this.checkDestroyed(),this._config}get container(){return this._container}get isDeleted(){return this._isDeleted}set isDeleted(e){this._isDeleted=e}checkDestroyed(){if(this.isDeleted)throw Ee.create("app-deleted",{appName:this._name})}}/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const _t=Ha;function Yc(t,e={}){typeof e!="object"&&(e={name:e});const n=Object.assign({name:bs,automaticDataCollectionEnabled:!1},e),s=n.name;if(typeof s!="string"||!s)throw Ee.create("bad-app-name",{appName:String(s)});const i=ct.get(s);if(i){if(lt(t,i.options)&&lt(n,i.config))return i;throw Ee.create("duplicate-app",{appName:s})}const r=new Yr(s);for(const o of Vt.values())r.addComponent(o);const a=new ja(t,n,r);return ct.set(s,a),a}function za(t=bs){const e=ct.get(t);if(!e)throw Ee.create("no-app",{appName:t});return e}function Ue(t,e,n){var s;let i=(s=Ba[t])!==null&&s!==void 0?s:t;n&&(i+=`-${n}`);const r=i.match(/\s|\//),a=e.match(/\s|\//);if(r||a){const o=[`Unable to register library "${i}" with version "${e}":`];r&&o.push(`library name "${i}" contains illegal characters (whitespace or "/")`),r&&a&&o.push("and"),a&&o.push(`version name "${e}" contains illegal characters (whitespace or "/")`),Ie.warn(o.join(" "));return}He(new Le(`${i}-version`,()=>({library:i,version:e}),"VERSION"))}/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Ga="firebase-heartbeat-database",qa=1,Be="firebase-heartbeat-store";let St=null;function Es(){return St||(St=ca(Ga,qa,{upgrade:(t,e)=>{switch(e){case 0:t.createObjectStore(Be)}}}).catch(t=>{throw Ee.create("idb-open",{originalErrorMessage:t.message})})),St}async function Ka(t){var e;try{return(await Es()).transaction(Be).objectStore(Be).get(ws(t))}catch(n){if(n instanceof me)Ie.warn(n.message);else{const s=Ee.create("idb-get",{originalErrorMessage:(e=n)===null||e===void 0?void 0:e.message});Ie.warn(s.message)}}}async function zn(t,e){var n;try{const i=(await Es()).transaction(Be,"readwrite");return await i.objectStore(Be).put(e,ws(t)),i.done}catch(s){if(s instanceof me)Ie.warn(s.message);else{const i=Ee.create("idb-set",{originalErrorMessage:(n=s)===null||n===void 0?void 0:n.message});Ie.warn(i.message)}}}function ws(t){return`${t.name}!${t.options.appId}`}/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Ja=1024,Xa=30*24*60*60*1e3;class Za{constructor(e){this.container=e,this._heartbeatsCache=null;const n=this.container.getProvider("app").getImmediate();this._storage=new Qa(n),this._heartbeatsCachePromise=this._storage.read().then(s=>(this._heartbeatsCache=s,s))}async triggerHeartbeat(){const n=this.container.getProvider("platform-logger").getImmediate().getPlatformInfoString(),s=Gn();if(this._heartbeatsCache===null&&(this._heartbeatsCache=await this._heartbeatsCachePromise),!(this._heartbeatsCache.lastSentHeartbeatDate===s||this._heartbeatsCache.heartbeats.some(i=>i.date===s)))return this._heartbeatsCache.heartbeats.push({date:s,agent:n}),this._heartbeatsCache.heartbeats=this._heartbeatsCache.heartbeats.filter(i=>{const r=new Date(i.date).valueOf();return Date.now()-r<=Xa}),this._storage.overwrite(this._heartbeatsCache)}async getHeartbeatsHeader(){if(this._heartbeatsCache===null&&await this._heartbeatsCachePromise,this._heartbeatsCache===null||this._heartbeatsCache.heartbeats.length===0)return"";const e=Gn(),{heartbeatsToSend:n,unsentEntries:s}=Ya(this._heartbeatsCache.heartbeats),i=gs(JSON.stringify({version:2,heartbeats:n}));return this._heartbeatsCache.lastSentHeartbeatDate=e,s.length>0?(this._heartbeatsCache.heartbeats=s,await this._storage.overwrite(this._heartbeatsCache)):(this._heartbeatsCache.heartbeats=[],this._storage.overwrite(this._heartbeatsCache)),i}}function Gn(){return new Date().toISOString().substring(0,10)}function Ya(t,e=Ja){const n=[];let s=t.slice();for(const i of t){const r=n.find(a=>a.agent===i.agent);if(r){if(r.dates.push(i.date),qn(n)>e){r.dates.pop();break}}else if(n.push({agent:i.agent,dates:[i.date]}),qn(n)>e){n.pop();break}s=s.slice(1)}return{heartbeatsToSend:n,unsentEntries:s}}class Qa{constructor(e){this.app=e,this._canUseIndexedDBPromise=this.runIndexedDBEnvironmentCheck()}async runIndexedDBEnvironmentCheck(){return Hr()?Br().then(()=>!0).catch(()=>!1):!1}async read(){return await this._canUseIndexedDBPromise?await Ka(this.app)||{heartbeats:[]}:{heartbeats:[]}}async overwrite(e){var n;if(await this._canUseIndexedDBPromise){const i=await this.read();return zn(this.app,{lastSentHeartbeatDate:(n=e.lastSentHeartbeatDate)!==null&&n!==void 0?n:i.lastSentHeartbeatDate,heartbeats:e.heartbeats})}else return}async add(e){var n;if(await this._canUseIndexedDBPromise){const i=await this.read();return zn(this.app,{lastSentHeartbeatDate:(n=e.lastSentHeartbeatDate)!==null&&n!==void 0?n:i.lastSentHeartbeatDate,heartbeats:[...i.heartbeats,...e.heartbeats]})}else return}}function qn(t){return gs(JSON.stringify({version:2,heartbeats:t})).length}/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function xa(t){He(new Le("platform-logger",e=>new fa(e),"PRIVATE")),He(new Le("heartbeat",e=>new Za(e),"PRIVATE")),Ue(Ut,jn,t),Ue(Ut,jn,"esm2017"),Ue("fire-js","")}xa("");function Zt(t,e){var n={};for(var s in t)Object.prototype.hasOwnProperty.call(t,s)&&e.indexOf(s)<0&&(n[s]=t[s]);if(t!=null&&typeof Object.getOwnPropertySymbols=="function")for(var i=0,s=Object.getOwnPropertySymbols(t);i<s.length;i++)e.indexOf(s[i])<0&&Object.prototype.propertyIsEnumerable.call(t,s[i])&&(n[s[i]]=t[s[i]]);return n}function Ts(){return{["dependent-sdk-initialized-before-auth"]:"Another Firebase SDK was initialized and is trying to use Auth before Auth is initialized. Please be sure to call `initializeAuth` or `getAuth` before starting any other Firebase SDK."}}const $a=Ts,ks=new Ge("auth","Firebase",Ts());/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Kn=new ps("@firebase/auth");function nt(t,...e){Kn.logLevel<=H.ERROR&&Kn.error(`Auth (${_t}): ${t}`,...e)}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Q(t,...e){throw Yt(t,...e)}function te(t,...e){return Yt(t,...e)}function eo(t,e,n){const s=Object.assign(Object.assign({},$a()),{[e]:n});return new Ge("auth","Firebase",s).create(e,{appName:t.name})}function Yt(t,...e){if(typeof t!="string"){const n=e[0],s=[...e.slice(1)];return s[0]&&(s[0].appName=t.name),t._errorFactory.create(n,...s)}return ks.create(t,...e)}function R(t,e,...n){if(!t)throw Yt(e,...n)}function se(t){const e="INTERNAL ASSERTION FAILED: "+t;throw nt(e),new Error(e)}function oe(t,e){t||se(e)}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Jn=new Map;function ie(t){oe(t instanceof Function,"Expected a class definition");let e=Jn.get(t);return e?(oe(e instanceof t,"Instance stored in cache mismatched with class"),e):(e=new t,Jn.set(t,e),e)}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function to(t,e){const n=Is(t,"auth");if(n.isInitialized()){const i=n.getImmediate(),r=n.getOptions();if(lt(r,e!=null?e:{}))return i;Q(i,"already-initialized")}return n.initialize({options:e})}function no(t,e){const n=(e==null?void 0:e.persistence)||[],s=(Array.isArray(n)?n:[n]).map(ie);e!=null&&e.errorMap&&t._updateErrorMap(e.errorMap),t._initializeWithPersistence(s,e==null?void 0:e.popupRedirectResolver)}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Ht(){var t;return typeof self<"u"&&((t=self.location)===null||t===void 0?void 0:t.href)||""}function so(){return Xn()==="http:"||Xn()==="https:"}function Xn(){var t;return typeof self<"u"&&((t=self.location)===null||t===void 0?void 0:t.protocol)||null}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function io(){return typeof navigator<"u"&&navigator&&"onLine"in navigator&&typeof navigator.onLine=="boolean"&&(so()||Nr()||"connection"in navigator)?navigator.onLine:!0}function ro(){if(typeof navigator>"u")return null;const t=navigator;return t.languages&&t.languages[0]||t.language||null}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Ke{constructor(e,n){this.shortDelay=e,this.longDelay=n,oe(n>e,"Short delay should be less than long delay!"),this.isMobile=Pr()||Ur()}get(){return io()?this.isMobile?this.longDelay:this.shortDelay:Math.min(5e3,this.shortDelay)}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Qt(t,e){oe(t.emulator,"Emulator should always be set here");const{url:n}=t.emulator;return e?`${n}${e.startsWith("/")?e.slice(1):e}`:n}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Cs{static initialize(e,n,s){this.fetchImpl=e,n&&(this.headersImpl=n),s&&(this.responseImpl=s)}static fetch(){if(this.fetchImpl)return this.fetchImpl;if(typeof self<"u"&&"fetch"in self)return self.fetch;se("Could not find fetch implementation, make sure you call FetchProvider.initialize() with an appropriate polyfill")}static headers(){if(this.headersImpl)return this.headersImpl;if(typeof self<"u"&&"Headers"in self)return self.Headers;se("Could not find Headers implementation, make sure you call FetchProvider.initialize() with an appropriate polyfill")}static response(){if(this.responseImpl)return this.responseImpl;if(typeof self<"u"&&"Response"in self)return self.Response;se("Could not find Response implementation, make sure you call FetchProvider.initialize() with an appropriate polyfill")}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const ao={CREDENTIAL_MISMATCH:"custom-token-mismatch",MISSING_CUSTOM_TOKEN:"internal-error",INVALID_IDENTIFIER:"invalid-email",MISSING_CONTINUE_URI:"internal-error",INVALID_PASSWORD:"wrong-password",MISSING_PASSWORD:"internal-error",EMAIL_EXISTS:"email-already-in-use",PASSWORD_LOGIN_DISABLED:"operation-not-allowed",INVALID_IDP_RESPONSE:"invalid-credential",INVALID_PENDING_TOKEN:"invalid-credential",FEDERATED_USER_ID_ALREADY_LINKED:"credential-already-in-use",MISSING_REQ_TYPE:"internal-error",EMAIL_NOT_FOUND:"user-not-found",RESET_PASSWORD_EXCEED_LIMIT:"too-many-requests",EXPIRED_OOB_CODE:"expired-action-code",INVALID_OOB_CODE:"invalid-action-code",MISSING_OOB_CODE:"internal-error",CREDENTIAL_TOO_OLD_LOGIN_AGAIN:"requires-recent-login",INVALID_ID_TOKEN:"invalid-user-token",TOKEN_EXPIRED:"user-token-expired",USER_NOT_FOUND:"user-token-expired",TOO_MANY_ATTEMPTS_TRY_LATER:"too-many-requests",INVALID_CODE:"invalid-verification-code",INVALID_SESSION_INFO:"invalid-verification-id",INVALID_TEMPORARY_PROOF:"invalid-credential",MISSING_SESSION_INFO:"missing-verification-id",SESSION_EXPIRED:"code-expired",MISSING_ANDROID_PACKAGE_NAME:"missing-android-pkg-name",UNAUTHORIZED_DOMAIN:"unauthorized-continue-uri",INVALID_OAUTH_CLIENT_ID:"invalid-oauth-client-id",ADMIN_ONLY_OPERATION:"admin-restricted-operation",INVALID_MFA_PENDING_CREDENTIAL:"invalid-multi-factor-session",MFA_ENROLLMENT_NOT_FOUND:"multi-factor-info-not-found",MISSING_MFA_ENROLLMENT_ID:"missing-multi-factor-info",MISSING_MFA_PENDING_CREDENTIAL:"missing-multi-factor-session",SECOND_FACTOR_EXISTS:"second-factor-already-in-use",SECOND_FACTOR_LIMIT_EXCEEDED:"maximum-second-factor-count-exceeded",BLOCKING_FUNCTION_ERROR_RESPONSE:"internal-error"};/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const oo=new Ke(3e4,6e4);function Je(t,e){return t.tenantId&&!e.tenantId?Object.assign(Object.assign({},e),{tenantId:t.tenantId}):e}async function Xe(t,e,n,s,i={}){return Ss(t,i,async()=>{let r={},a={};s&&(e==="GET"?a=s:r={body:JSON.stringify(s)});const o=qe(Object.assign({key:t.config.apiKey},a)).slice(1),l=await t._getAdditionalHeaders();return l["Content-Type"]="application/json",t.languageCode&&(l["X-Firebase-Locale"]=t.languageCode),Cs.fetch()(As(t,t.config.apiHost,n,o),Object.assign({method:e,headers:l,referrerPolicy:"no-referrer"},r))})}async function Ss(t,e,n){t._canInitEmulator=!1;const s=Object.assign(Object.assign({},ao),e);try{const i=new lo(t),r=await Promise.race([n(),i.promise]);i.clearNetworkTimeout();const a=await r.json();if("needConfirmation"in a)throw tt(t,"account-exists-with-different-credential",a);if(r.ok&&!("errorMessage"in a))return a;{const o=r.ok?a.errorMessage:a.error.message,[l,c]=o.split(" : ");if(l==="FEDERATED_USER_ID_ALREADY_LINKED")throw tt(t,"credential-already-in-use",a);if(l==="EMAIL_EXISTS")throw tt(t,"email-already-in-use",a);if(l==="USER_DISABLED")throw tt(t,"user-disabled",a);const u=s[l]||l.toLowerCase().replace(/[_\s]+/g,"-");if(c)throw eo(t,u,c);Q(t,u)}}catch(i){if(i instanceof me)throw i;Q(t,"network-request-failed")}}async function Ze(t,e,n,s,i={}){const r=await Xe(t,e,n,s,i);return"mfaPendingCredential"in r&&Q(t,"multi-factor-auth-required",{_serverResponse:r}),r}function As(t,e,n,s){const i=`${e}${n}?${s}`;return t.config.emulator?Qt(t.config,i):`${t.config.apiScheme}://${i}`}class lo{constructor(e){this.auth=e,this.timer=null,this.promise=new Promise((n,s)=>{this.timer=setTimeout(()=>s(te(this.auth,"network-request-failed")),oo.get())})}clearNetworkTimeout(){clearTimeout(this.timer)}}function tt(t,e,n){const s={appName:t.name};n.email&&(s.email=n.email),n.phoneNumber&&(s.phoneNumber=n.phoneNumber);const i=te(t,e,s);return i.customData._tokenResponse=n,i}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function co(t,e){return Xe(t,"POST","/v1/accounts:delete",e)}async function uo(t,e){return Xe(t,"POST","/v1/accounts:lookup",e)}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Ve(t){if(!!t)try{const e=new Date(Number(t));if(!isNaN(e.getTime()))return e.toUTCString()}catch{}}async function fo(t,e=!1){const n=le(t),s=await n.getIdToken(e),i=xt(s);R(i&&i.exp&&i.auth_time&&i.iat,n.auth,"internal-error");const r=typeof i.firebase=="object"?i.firebase:void 0,a=r==null?void 0:r.sign_in_provider;return{claims:i,token:s,authTime:Ve(At(i.auth_time)),issuedAtTime:Ve(At(i.iat)),expirationTime:Ve(At(i.exp)),signInProvider:a||null,signInSecondFactor:(r==null?void 0:r.sign_in_second_factor)||null}}function At(t){return Number(t)*1e3}function xt(t){var e;const[n,s,i]=t.split(".");if(n===void 0||s===void 0||i===void 0)return nt("JWT malformed, contained fewer than 3 sections"),null;try{const r=Dr(s);return r?JSON.parse(r):(nt("Failed to decode base64 JWT payload"),null)}catch(r){return nt("Caught error parsing JWT payload as JSON",(e=r)===null||e===void 0?void 0:e.toString()),null}}function ho(t){const e=xt(t);return R(e,"internal-error"),R(typeof e.exp<"u","internal-error"),R(typeof e.iat<"u","internal-error"),Number(e.exp)-Number(e.iat)}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Fe(t,e,n=!1){if(n)return e;try{return await e}catch(s){throw s instanceof me&&_o(s)&&t.auth.currentUser===t&&await t.auth.signOut(),s}}function _o({code:t}){return t==="auth/user-disabled"||t==="auth/user-token-expired"}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class mo{constructor(e){this.user=e,this.isRunning=!1,this.timerId=null,this.errorBackoff=3e4}_start(){this.isRunning||(this.isRunning=!0,this.schedule())}_stop(){!this.isRunning||(this.isRunning=!1,this.timerId!==null&&clearTimeout(this.timerId))}getInterval(e){var n;if(e){const s=this.errorBackoff;return this.errorBackoff=Math.min(this.errorBackoff*2,96e4),s}else{this.errorBackoff=3e4;const i=((n=this.user.stsTokenManager.expirationTime)!==null&&n!==void 0?n:0)-Date.now()-3e5;return Math.max(0,i)}}schedule(e=!1){if(!this.isRunning)return;const n=this.getInterval(e);this.timerId=setTimeout(async()=>{await this.iteration()},n)}async iteration(){var e;try{await this.user.getIdToken(!0)}catch(n){((e=n)===null||e===void 0?void 0:e.code)==="auth/network-request-failed"&&this.schedule(!0);return}this.schedule()}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Ls{constructor(e,n){this.createdAt=e,this.lastLoginAt=n,this._initializeTime()}_initializeTime(){this.lastSignInTime=Ve(this.lastLoginAt),this.creationTime=Ve(this.createdAt)}_copy(e){this.createdAt=e.createdAt,this.lastLoginAt=e.lastLoginAt,this._initializeTime()}toJSON(){return{createdAt:this.createdAt,lastLoginAt:this.lastLoginAt}}}/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function ut(t){var e;const n=t.auth,s=await t.getIdToken(),i=await Fe(t,uo(n,{idToken:s}));R(i==null?void 0:i.users.length,n,"internal-error");const r=i.users[0];t._notifyReloadListener(r);const a=!((e=r.providerUserInfo)===null||e===void 0)&&e.length?vo(r.providerUserInfo):[],o=po(t.providerData,a),l=t.isAnonymous,c=!(t.email&&r.passwordHash)&&!(o!=null&&o.length),u=l?c:!1,f={uid:r.localId,displayName:r.displayName||null,photoURL:r.photoUrl||null,email:r.email||null,emailVerified:r.emailVerified||!1,phoneNumber:r.phoneNumber||null,tenantId:r.tenantId||null,providerData:o,metadata:new Ls(r.createdAt,r.lastLoginAt),isAnonymous:u};Object.assign(t,f)}async function go(t){const e=le(t);await ut(e),await e.auth._persistUserIfCurrent(e),e.auth._notifyListenersIfCurrent(e)}function po(t,e){return[...t.filter(s=>!e.some(i=>i.providerId===s.providerId)),...e]}function vo(t){return t.map(e=>{var{providerId:n}=e,s=Zt(e,["providerId"]);return{providerId:n,uid:s.rawId||"",displayName:s.displayName||null,email:s.email||null,phoneNumber:s.phoneNumber||null,photoURL:s.photoUrl||null}})}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function yo(t,e){const n=await Ss(t,{},async()=>{const s=qe({grant_type:"refresh_token",refresh_token:e}).slice(1),{tokenApiHost:i,apiKey:r}=t.config,a=As(t,i,"/v1/token",`key=${r}`),o=await t._getAdditionalHeaders();return o["Content-Type"]="application/x-www-form-urlencoded",Cs.fetch()(a,{method:"POST",headers:o,body:s})});return{accessToken:n.access_token,expiresIn:n.expires_in,refreshToken:n.refresh_token}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class We{constructor(){this.refreshToken=null,this.accessToken=null,this.expirationTime=null}get isExpired(){return!this.expirationTime||Date.now()>this.expirationTime-3e4}updateFromServerResponse(e){R(e.idToken,"internal-error"),R(typeof e.idToken<"u","internal-error"),R(typeof e.refreshToken<"u","internal-error");const n="expiresIn"in e&&typeof e.expiresIn<"u"?Number(e.expiresIn):ho(e.idToken);this.updateTokensAndExpiration(e.idToken,e.refreshToken,n)}async getToken(e,n=!1){return R(!this.accessToken||this.refreshToken,e,"user-token-expired"),!n&&this.accessToken&&!this.isExpired?this.accessToken:this.refreshToken?(await this.refresh(e,this.refreshToken),this.accessToken):null}clearRefreshToken(){this.refreshToken=null}async refresh(e,n){const{accessToken:s,refreshToken:i,expiresIn:r}=await yo(e,n);this.updateTokensAndExpiration(s,i,Number(r))}updateTokensAndExpiration(e,n,s){this.refreshToken=n||null,this.accessToken=e||null,this.expirationTime=Date.now()+s*1e3}static fromJSON(e,n){const{refreshToken:s,accessToken:i,expirationTime:r}=n,a=new We;return s&&(R(typeof s=="string","internal-error",{appName:e}),a.refreshToken=s),i&&(R(typeof i=="string","internal-error",{appName:e}),a.accessToken=i),r&&(R(typeof r=="number","internal-error",{appName:e}),a.expirationTime=r),a}toJSON(){return{refreshToken:this.refreshToken,accessToken:this.accessToken,expirationTime:this.expirationTime}}_assign(e){this.accessToken=e.accessToken,this.refreshToken=e.refreshToken,this.expirationTime=e.expirationTime}_clone(){return Object.assign(new We,this.toJSON())}_performRefresh(){return se("not implemented")}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function ce(t,e){R(typeof t=="string"||typeof t>"u","internal-error",{appName:e})}class ye{constructor(e){var{uid:n,auth:s,stsTokenManager:i}=e,r=Zt(e,["uid","auth","stsTokenManager"]);this.providerId="firebase",this.proactiveRefresh=new mo(this),this.reloadUserInfo=null,this.reloadListener=null,this.uid=n,this.auth=s,this.stsTokenManager=i,this.accessToken=i.accessToken,this.displayName=r.displayName||null,this.email=r.email||null,this.emailVerified=r.emailVerified||!1,this.phoneNumber=r.phoneNumber||null,this.photoURL=r.photoURL||null,this.isAnonymous=r.isAnonymous||!1,this.tenantId=r.tenantId||null,this.providerData=r.providerData?[...r.providerData]:[],this.metadata=new Ls(r.createdAt||void 0,r.lastLoginAt||void 0)}async getIdToken(e){const n=await Fe(this,this.stsTokenManager.getToken(this.auth,e));return R(n,this.auth,"internal-error"),this.accessToken!==n&&(this.accessToken=n,await this.auth._persistUserIfCurrent(this),this.auth._notifyListenersIfCurrent(this)),n}getIdTokenResult(e){return fo(this,e)}reload(){return go(this)}_assign(e){this!==e&&(R(this.uid===e.uid,this.auth,"internal-error"),this.displayName=e.displayName,this.photoURL=e.photoURL,this.email=e.email,this.emailVerified=e.emailVerified,this.phoneNumber=e.phoneNumber,this.isAnonymous=e.isAnonymous,this.tenantId=e.tenantId,this.providerData=e.providerData.map(n=>Object.assign({},n)),this.metadata._copy(e.metadata),this.stsTokenManager._assign(e.stsTokenManager))}_clone(e){return new ye(Object.assign(Object.assign({},this),{auth:e,stsTokenManager:this.stsTokenManager._clone()}))}_onReload(e){R(!this.reloadListener,this.auth,"internal-error"),this.reloadListener=e,this.reloadUserInfo&&(this._notifyReloadListener(this.reloadUserInfo),this.reloadUserInfo=null)}_notifyReloadListener(e){this.reloadListener?this.reloadListener(e):this.reloadUserInfo=e}_startProactiveRefresh(){this.proactiveRefresh._start()}_stopProactiveRefresh(){this.proactiveRefresh._stop()}async _updateTokensIfNecessary(e,n=!1){let s=!1;e.idToken&&e.idToken!==this.stsTokenManager.accessToken&&(this.stsTokenManager.updateFromServerResponse(e),s=!0),n&&await ut(this),await this.auth._persistUserIfCurrent(this),s&&this.auth._notifyListenersIfCurrent(this)}async delete(){const e=await this.getIdToken();return await Fe(this,co(this.auth,{idToken:e})),this.stsTokenManager.clearRefreshToken(),this.auth.signOut()}toJSON(){return Object.assign(Object.assign({uid:this.uid,email:this.email||void 0,emailVerified:this.emailVerified,displayName:this.displayName||void 0,isAnonymous:this.isAnonymous,photoURL:this.photoURL||void 0,phoneNumber:this.phoneNumber||void 0,tenantId:this.tenantId||void 0,providerData:this.providerData.map(e=>Object.assign({},e)),stsTokenManager:this.stsTokenManager.toJSON(),_redirectEventId:this._redirectEventId},this.metadata.toJSON()),{apiKey:this.auth.config.apiKey,appName:this.auth.name})}get refreshToken(){return this.stsTokenManager.refreshToken||""}static _fromJSON(e,n){var s,i,r,a,o,l,c,u;const f=(s=n.displayName)!==null&&s!==void 0?s:void 0,m=(i=n.email)!==null&&i!==void 0?i:void 0,d=(r=n.phoneNumber)!==null&&r!==void 0?r:void 0,h=(a=n.photoURL)!==null&&a!==void 0?a:void 0,y=(o=n.tenantId)!==null&&o!==void 0?o:void 0,g=(l=n._redirectEventId)!==null&&l!==void 0?l:void 0,p=(c=n.createdAt)!==null&&c!==void 0?c:void 0,A=(u=n.lastLoginAt)!==null&&u!==void 0?u:void 0,{uid:k,emailVerified:D,isAnonymous:v,providerData:L,stsTokenManager:w}=n;R(k&&w,e,"internal-error");const _=We.fromJSON(this.name,w);R(typeof k=="string",e,"internal-error"),ce(f,e.name),ce(m,e.name),R(typeof D=="boolean",e,"internal-error"),R(typeof v=="boolean",e,"internal-error"),ce(d,e.name),ce(h,e.name),ce(y,e.name),ce(g,e.name),ce(p,e.name),ce(A,e.name);const E=new ye({uid:k,auth:e,email:m,emailVerified:D,displayName:f,isAnonymous:v,photoURL:h,phoneNumber:d,tenantId:y,stsTokenManager:_,createdAt:p,lastLoginAt:A});return L&&Array.isArray(L)&&(E.providerData=L.map(C=>Object.assign({},C))),g&&(E._redirectEventId=g),E}static async _fromIdTokenResponse(e,n,s=!1){const i=new We;i.updateFromServerResponse(n);const r=new ye({uid:n.localId,auth:e,stsTokenManager:i,isAnonymous:s});return await ut(r),r}}/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Rs{constructor(){this.type="NONE",this.storage={}}async _isAvailable(){return!0}async _set(e,n){this.storage[e]=n}async _get(e){const n=this.storage[e];return n===void 0?null:n}async _remove(e){delete this.storage[e]}_addListener(e,n){}_removeListener(e,n){}}Rs.type="NONE";const Zn=Rs;/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function st(t,e,n){return`firebase:${t}:${e}:${n}`}class Ce{constructor(e,n,s){this.persistence=e,this.auth=n,this.userKey=s;const{config:i,name:r}=this.auth;this.fullUserKey=st(this.userKey,i.apiKey,r),this.fullPersistenceKey=st("persistence",i.apiKey,r),this.boundEventHandler=n._onStorageEvent.bind(n),this.persistence._addListener(this.fullUserKey,this.boundEventHandler)}setCurrentUser(e){return this.persistence._set(this.fullUserKey,e.toJSON())}async getCurrentUser(){const e=await this.persistence._get(this.fullUserKey);return e?ye._fromJSON(this.auth,e):null}removeCurrentUser(){return this.persistence._remove(this.fullUserKey)}savePersistenceForRedirect(){return this.persistence._set(this.fullPersistenceKey,this.persistence.type)}async setPersistence(e){if(this.persistence===e)return;const n=await this.getCurrentUser();if(await this.removeCurrentUser(),this.persistence=e,n)return this.setCurrentUser(n)}delete(){this.persistence._removeListener(this.fullUserKey,this.boundEventHandler)}static async create(e,n,s="authUser"){if(!n.length)return new Ce(ie(Zn),e,s);const i=(await Promise.all(n.map(async c=>{if(await c._isAvailable())return c}))).filter(c=>c);let r=i[0]||ie(Zn);const a=st(s,e.config.apiKey,e.name);let o=null;for(const c of n)try{const u=await c._get(a);if(u){const f=ye._fromJSON(e,u);c!==r&&(o=f),r=c;break}}catch{}const l=i.filter(c=>c._shouldAllowMigration);return!r._shouldAllowMigration||!l.length?new Ce(r,e,s):(r=l[0],o&&await r._set(a,o.toJSON()),await Promise.all(n.map(async c=>{if(c!==r)try{await c._remove(a)}catch{}})),new Ce(r,e,s))}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Yn(t){const e=t.toLowerCase();if(e.includes("opera/")||e.includes("opr/")||e.includes("opios/"))return"Opera";if(Ms(e))return"IEMobile";if(e.includes("msie")||e.includes("trident/"))return"IE";if(e.includes("edge/"))return"Edge";if(Os(e))return"Firefox";if(e.includes("silk/"))return"Silk";if(Ns(e))return"Blackberry";if(Us(e))return"Webos";if($t(e))return"Safari";if((e.includes("chrome/")||Ds(e))&&!e.includes("edge/"))return"Chrome";if(Ps(e))return"Android";{const n=/([a-zA-Z\d\.]+)\/[a-zA-Z\d\.]*$/,s=t.match(n);if((s==null?void 0:s.length)===2)return s[1]}return"Other"}function Os(t=J()){return/firefox\//i.test(t)}function $t(t=J()){const e=t.toLowerCase();return e.includes("safari/")&&!e.includes("chrome/")&&!e.includes("crios/")&&!e.includes("android")}function Ds(t=J()){return/crios\//i.test(t)}function Ms(t=J()){return/iemobile/i.test(t)}function Ps(t=J()){return/android/i.test(t)}function Ns(t=J()){return/blackberry/i.test(t)}function Us(t=J()){return/webos/i.test(t)}function mt(t=J()){return/iphone|ipad|ipod/i.test(t)||/macintosh/i.test(t)&&/mobile/i.test(t)}function bo(t=J()){var e;return mt(t)&&!!(!((e=window.navigator)===null||e===void 0)&&e.standalone)}function Io(){return Vr()&&document.documentMode===10}function Vs(t=J()){return mt(t)||Ps(t)||Us(t)||Ns(t)||/windows phone/i.test(t)||Ms(t)}function Eo(){try{return!!(window&&window!==window.top)}catch{return!1}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Hs(t,e=[]){let n;switch(t){case"Browser":n=Yn(J());break;case"Worker":n=`${Yn(J())}-${t}`;break;default:n=t}const s=e.length?e.join(","):"FirebaseCore-web";return`${n}/JsCore/${_t}/${s}`}/**
 * @license
 * Copyright 2022 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class wo{constructor(e){this.auth=e,this.queue=[]}pushCallback(e,n){const s=r=>new Promise((a,o)=>{try{const l=e(r);a(l)}catch(l){o(l)}});s.onAbort=n,this.queue.push(s);const i=this.queue.length-1;return()=>{this.queue[i]=()=>Promise.resolve()}}async runMiddleware(e){var n;if(this.auth.currentUser===e)return;const s=[];try{for(const i of this.queue)await i(e),i.onAbort&&s.push(i.onAbort)}catch(i){s.reverse();for(const r of s)try{r()}catch{}throw this.auth._errorFactory.create("login-blocked",{originalMessage:(n=i)===null||n===void 0?void 0:n.message})}}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class To{constructor(e,n,s){this.app=e,this.heartbeatServiceProvider=n,this.config=s,this.currentUser=null,this.emulatorConfig=null,this.operations=Promise.resolve(),this.authStateSubscription=new Qn(this),this.idTokenSubscription=new Qn(this),this.beforeStateQueue=new wo(this),this.redirectUser=null,this.isProactiveRefreshEnabled=!1,this._canInitEmulator=!0,this._isInitialized=!1,this._deleted=!1,this._initializationPromise=null,this._popupRedirectResolver=null,this._errorFactory=ks,this.lastNotifiedUid=void 0,this.languageCode=null,this.tenantId=null,this.settings={appVerificationDisabledForTesting:!1},this.frameworks=[],this.name=e.name,this.clientVersion=s.sdkClientVersion}_initializeWithPersistence(e,n){return n&&(this._popupRedirectResolver=ie(n)),this._initializationPromise=this.queue(async()=>{var s,i;if(!this._deleted&&(this.persistenceManager=await Ce.create(this,e),!this._deleted)){if(!((s=this._popupRedirectResolver)===null||s===void 0)&&s._shouldInitProactively)try{await this._popupRedirectResolver._initialize(this)}catch{}await this.initializeCurrentUser(n),this.lastNotifiedUid=((i=this.currentUser)===null||i===void 0?void 0:i.uid)||null,!this._deleted&&(this._isInitialized=!0)}}),this._initializationPromise}async _onStorageEvent(){if(this._deleted)return;const e=await this.assertedPersistence.getCurrentUser();if(!(!this.currentUser&&!e)){if(this.currentUser&&e&&this.currentUser.uid===e.uid){this._currentUser._assign(e),await this.currentUser.getIdToken();return}await this._updateCurrentUser(e,!0)}}async initializeCurrentUser(e){var n;const s=await this.assertedPersistence.getCurrentUser();let i=s,r=!1;if(e&&this.config.authDomain){await this.getOrInitRedirectPersistenceManager();const a=(n=this.redirectUser)===null||n===void 0?void 0:n._redirectEventId,o=i==null?void 0:i._redirectEventId,l=await this.tryRedirectSignIn(e);(!a||a===o)&&(l==null?void 0:l.user)&&(i=l.user,r=!0)}if(!i)return this.directlySetCurrentUser(null);if(!i._redirectEventId){if(r)try{await this.beforeStateQueue.runMiddleware(i)}catch(a){i=s,this._popupRedirectResolver._overrideRedirectResult(this,()=>Promise.reject(a))}return i?this.reloadAndSetCurrentUserOrClear(i):this.directlySetCurrentUser(null)}return R(this._popupRedirectResolver,this,"argument-error"),await this.getOrInitRedirectPersistenceManager(),this.redirectUser&&this.redirectUser._redirectEventId===i._redirectEventId?this.directlySetCurrentUser(i):this.reloadAndSetCurrentUserOrClear(i)}async tryRedirectSignIn(e){let n=null;try{n=await this._popupRedirectResolver._completeRedirectFn(this,e,!0)}catch{await this._setRedirectUser(null)}return n}async reloadAndSetCurrentUserOrClear(e){var n;try{await ut(e)}catch(s){if(((n=s)===null||n===void 0?void 0:n.code)!=="auth/network-request-failed")return this.directlySetCurrentUser(null)}return this.directlySetCurrentUser(e)}useDeviceLanguage(){this.languageCode=ro()}async _delete(){this._deleted=!0}async updateCurrentUser(e){const n=e?le(e):null;return n&&R(n.auth.config.apiKey===this.config.apiKey,this,"invalid-user-token"),this._updateCurrentUser(n&&n._clone(this))}async _updateCurrentUser(e,n=!1){if(!this._deleted)return e&&R(this.tenantId===e.tenantId,this,"tenant-id-mismatch"),n||await this.beforeStateQueue.runMiddleware(e),this.queue(async()=>{await this.directlySetCurrentUser(e),this.notifyAuthListeners()})}async signOut(){return await this.beforeStateQueue.runMiddleware(null),(this.redirectPersistenceManager||this._popupRedirectResolver)&&await this._setRedirectUser(null),this._updateCurrentUser(null,!0)}setPersistence(e){return this.queue(async()=>{await this.assertedPersistence.setPersistence(ie(e))})}_getPersistence(){return this.assertedPersistence.persistence.type}_updateErrorMap(e){this._errorFactory=new Ge("auth","Firebase",e())}onAuthStateChanged(e,n,s){return this.registerStateListener(this.authStateSubscription,e,n,s)}beforeAuthStateChanged(e,n){return this.beforeStateQueue.pushCallback(e,n)}onIdTokenChanged(e,n,s){return this.registerStateListener(this.idTokenSubscription,e,n,s)}toJSON(){var e;return{apiKey:this.config.apiKey,authDomain:this.config.authDomain,appName:this.name,currentUser:(e=this._currentUser)===null||e===void 0?void 0:e.toJSON()}}async _setRedirectUser(e,n){const s=await this.getOrInitRedirectPersistenceManager(n);return e===null?s.removeCurrentUser():s.setCurrentUser(e)}async getOrInitRedirectPersistenceManager(e){if(!this.redirectPersistenceManager){const n=e&&ie(e)||this._popupRedirectResolver;R(n,this,"argument-error"),this.redirectPersistenceManager=await Ce.create(this,[ie(n._redirectPersistence)],"redirectUser"),this.redirectUser=await this.redirectPersistenceManager.getCurrentUser()}return this.redirectPersistenceManager}async _redirectUserForId(e){var n,s;return this._isInitialized&&await this.queue(async()=>{}),((n=this._currentUser)===null||n===void 0?void 0:n._redirectEventId)===e?this._currentUser:((s=this.redirectUser)===null||s===void 0?void 0:s._redirectEventId)===e?this.redirectUser:null}async _persistUserIfCurrent(e){if(e===this.currentUser)return this.queue(async()=>this.directlySetCurrentUser(e))}_notifyListenersIfCurrent(e){e===this.currentUser&&this.notifyAuthListeners()}_key(){return`${this.config.authDomain}:${this.config.apiKey}:${this.name}`}_startProactiveRefresh(){this.isProactiveRefreshEnabled=!0,this.currentUser&&this._currentUser._startProactiveRefresh()}_stopProactiveRefresh(){this.isProactiveRefreshEnabled=!1,this.currentUser&&this._currentUser._stopProactiveRefresh()}get _currentUser(){return this.currentUser}notifyAuthListeners(){var e,n;if(!this._isInitialized)return;this.idTokenSubscription.next(this.currentUser);const s=(n=(e=this.currentUser)===null||e===void 0?void 0:e.uid)!==null&&n!==void 0?n:null;this.lastNotifiedUid!==s&&(this.lastNotifiedUid=s,this.authStateSubscription.next(this.currentUser))}registerStateListener(e,n,s,i){if(this._deleted)return()=>{};const r=typeof n=="function"?n:n.next.bind(n),a=this._isInitialized?Promise.resolve():this._initializationPromise;return R(a,this,"internal-error"),a.then(()=>r(this.currentUser)),typeof n=="function"?e.addObserver(n,s,i):e.addObserver(n)}async directlySetCurrentUser(e){this.currentUser&&this.currentUser!==e&&this._currentUser._stopProactiveRefresh(),e&&this.isProactiveRefreshEnabled&&e._startProactiveRefresh(),this.currentUser=e,e?await this.assertedPersistence.setCurrentUser(e):await this.assertedPersistence.removeCurrentUser()}queue(e){return this.operations=this.operations.then(e,e),this.operations}get assertedPersistence(){return R(this.persistenceManager,this,"internal-error"),this.persistenceManager}_logFramework(e){!e||this.frameworks.includes(e)||(this.frameworks.push(e),this.frameworks.sort(),this.clientVersion=Hs(this.config.clientPlatform,this._getFrameworks()))}_getFrameworks(){return this.frameworks}async _getAdditionalHeaders(){var e;const n={["X-Client-Version"]:this.clientVersion};this.app.options.appId&&(n["X-Firebase-gmpid"]=this.app.options.appId);const s=await((e=this.heartbeatServiceProvider.getImmediate({optional:!0}))===null||e===void 0?void 0:e.getHeartbeatsHeader());return s&&(n["X-Firebase-Client"]=s),n}}function gt(t){return le(t)}class Qn{constructor(e){this.auth=e,this.observer=null,this.addObserver=Gr(n=>this.observer=n)}get next(){return R(this.observer,this.auth,"internal-error"),this.observer.next.bind(this.observer)}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class en{constructor(e,n){this.providerId=e,this.signInMethod=n}toJSON(){return se("not implemented")}_getIdTokenResponse(e){return se("not implemented")}_linkToIdToken(e,n){return se("not implemented")}_getReauthenticationResolver(e){return se("not implemented")}}async function ko(t,e){return Xe(t,"POST","/v1/accounts:update",e)}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Co(t,e){return Ze(t,"POST","/v1/accounts:signInWithPassword",Je(t,e))}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function So(t,e){return Ze(t,"POST","/v1/accounts:signInWithEmailLink",Je(t,e))}async function Ao(t,e){return Ze(t,"POST","/v1/accounts:signInWithEmailLink",Je(t,e))}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class je extends en{constructor(e,n,s,i=null){super("password",s),this._email=e,this._password=n,this._tenantId=i}static _fromEmailAndPassword(e,n){return new je(e,n,"password")}static _fromEmailAndCode(e,n,s=null){return new je(e,n,"emailLink",s)}toJSON(){return{email:this._email,password:this._password,signInMethod:this.signInMethod,tenantId:this._tenantId}}static fromJSON(e){const n=typeof e=="string"?JSON.parse(e):e;if((n==null?void 0:n.email)&&(n==null?void 0:n.password)){if(n.signInMethod==="password")return this._fromEmailAndPassword(n.email,n.password);if(n.signInMethod==="emailLink")return this._fromEmailAndCode(n.email,n.password,n.tenantId)}return null}async _getIdTokenResponse(e){switch(this.signInMethod){case"password":return Co(e,{returnSecureToken:!0,email:this._email,password:this._password});case"emailLink":return So(e,{email:this._email,oobCode:this._password});default:Q(e,"internal-error")}}async _linkToIdToken(e,n){switch(this.signInMethod){case"password":return ko(e,{idToken:n,returnSecureToken:!0,email:this._email,password:this._password});case"emailLink":return Ao(e,{idToken:n,email:this._email,oobCode:this._password});default:Q(e,"internal-error")}}_getReauthenticationResolver(e){return this._getIdTokenResponse(e)}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Se(t,e){return Ze(t,"POST","/v1/accounts:signInWithIdp",Je(t,e))}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Lo="http://localhost";class we extends en{constructor(){super(...arguments),this.pendingToken=null}static _fromParams(e){const n=new we(e.providerId,e.signInMethod);return e.idToken||e.accessToken?(e.idToken&&(n.idToken=e.idToken),e.accessToken&&(n.accessToken=e.accessToken),e.nonce&&!e.pendingToken&&(n.nonce=e.nonce),e.pendingToken&&(n.pendingToken=e.pendingToken)):e.oauthToken&&e.oauthTokenSecret?(n.accessToken=e.oauthToken,n.secret=e.oauthTokenSecret):Q("argument-error"),n}toJSON(){return{idToken:this.idToken,accessToken:this.accessToken,secret:this.secret,nonce:this.nonce,pendingToken:this.pendingToken,providerId:this.providerId,signInMethod:this.signInMethod}}static fromJSON(e){const n=typeof e=="string"?JSON.parse(e):e,{providerId:s,signInMethod:i}=n,r=Zt(n,["providerId","signInMethod"]);if(!s||!i)return null;const a=new we(s,i);return a.idToken=r.idToken||void 0,a.accessToken=r.accessToken||void 0,a.secret=r.secret,a.nonce=r.nonce,a.pendingToken=r.pendingToken||null,a}_getIdTokenResponse(e){const n=this.buildRequest();return Se(e,n)}_linkToIdToken(e,n){const s=this.buildRequest();return s.idToken=n,Se(e,s)}_getReauthenticationResolver(e){const n=this.buildRequest();return n.autoCreate=!1,Se(e,n)}buildRequest(){const e={requestUri:Lo,returnSecureToken:!0};if(this.pendingToken)e.pendingToken=this.pendingToken;else{const n={};this.idToken&&(n.id_token=this.idToken),this.accessToken&&(n.access_token=this.accessToken),this.secret&&(n.oauth_token_secret=this.secret),n.providerId=this.providerId,this.nonce&&!this.pendingToken&&(n.nonce=this.nonce),e.postBody=qe(n)}return e}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Ro(t){switch(t){case"recoverEmail":return"RECOVER_EMAIL";case"resetPassword":return"PASSWORD_RESET";case"signIn":return"EMAIL_SIGNIN";case"verifyEmail":return"VERIFY_EMAIL";case"verifyAndChangeEmail":return"VERIFY_AND_CHANGE_EMAIL";case"revertSecondFactorAddition":return"REVERT_SECOND_FACTOR_ADDITION";default:return null}}function Oo(t){const e=Pe(Ne(t)).link,n=e?Pe(Ne(e)).deep_link_id:null,s=Pe(Ne(t)).deep_link_id;return(s?Pe(Ne(s)).link:null)||s||n||e||t}class tn{constructor(e){var n,s,i,r,a,o;const l=Pe(Ne(e)),c=(n=l.apiKey)!==null&&n!==void 0?n:null,u=(s=l.oobCode)!==null&&s!==void 0?s:null,f=Ro((i=l.mode)!==null&&i!==void 0?i:null);R(c&&u&&f,"argument-error"),this.apiKey=c,this.operation=f,this.code=u,this.continueUrl=(r=l.continueUrl)!==null&&r!==void 0?r:null,this.languageCode=(a=l.languageCode)!==null&&a!==void 0?a:null,this.tenantId=(o=l.tenantId)!==null&&o!==void 0?o:null}static parseLink(e){const n=Oo(e);try{return new tn(n)}catch{return null}}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Oe{constructor(){this.providerId=Oe.PROVIDER_ID}static credential(e,n){return je._fromEmailAndPassword(e,n)}static credentialWithLink(e,n){const s=tn.parseLink(n);return R(s,"argument-error"),je._fromEmailAndCode(e,s.code,s.tenantId)}}Oe.PROVIDER_ID="password";Oe.EMAIL_PASSWORD_SIGN_IN_METHOD="password";Oe.EMAIL_LINK_SIGN_IN_METHOD="emailLink";/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Bs{constructor(e){this.providerId=e,this.defaultLanguageCode=null,this.customParameters={}}setDefaultLanguage(e){this.defaultLanguageCode=e}setCustomParameters(e){return this.customParameters=e,this}getCustomParameters(){return this.customParameters}}/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Ye extends Bs{constructor(){super(...arguments),this.scopes=[]}addScope(e){return this.scopes.includes(e)||this.scopes.push(e),this}getScopes(){return[...this.scopes]}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class ue extends Ye{constructor(){super("facebook.com")}static credential(e){return we._fromParams({providerId:ue.PROVIDER_ID,signInMethod:ue.FACEBOOK_SIGN_IN_METHOD,accessToken:e})}static credentialFromResult(e){return ue.credentialFromTaggedObject(e)}static credentialFromError(e){return ue.credentialFromTaggedObject(e.customData||{})}static credentialFromTaggedObject({_tokenResponse:e}){if(!e||!("oauthAccessToken"in e)||!e.oauthAccessToken)return null;try{return ue.credential(e.oauthAccessToken)}catch{return null}}}ue.FACEBOOK_SIGN_IN_METHOD="facebook.com";ue.PROVIDER_ID="facebook.com";/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class de extends Ye{constructor(){super("google.com"),this.addScope("profile")}static credential(e,n){return we._fromParams({providerId:de.PROVIDER_ID,signInMethod:de.GOOGLE_SIGN_IN_METHOD,idToken:e,accessToken:n})}static credentialFromResult(e){return de.credentialFromTaggedObject(e)}static credentialFromError(e){return de.credentialFromTaggedObject(e.customData||{})}static credentialFromTaggedObject({_tokenResponse:e}){if(!e)return null;const{oauthIdToken:n,oauthAccessToken:s}=e;if(!n&&!s)return null;try{return de.credential(n,s)}catch{return null}}}de.GOOGLE_SIGN_IN_METHOD="google.com";de.PROVIDER_ID="google.com";/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class fe extends Ye{constructor(){super("github.com")}static credential(e){return we._fromParams({providerId:fe.PROVIDER_ID,signInMethod:fe.GITHUB_SIGN_IN_METHOD,accessToken:e})}static credentialFromResult(e){return fe.credentialFromTaggedObject(e)}static credentialFromError(e){return fe.credentialFromTaggedObject(e.customData||{})}static credentialFromTaggedObject({_tokenResponse:e}){if(!e||!("oauthAccessToken"in e)||!e.oauthAccessToken)return null;try{return fe.credential(e.oauthAccessToken)}catch{return null}}}fe.GITHUB_SIGN_IN_METHOD="github.com";fe.PROVIDER_ID="github.com";/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class he extends Ye{constructor(){super("twitter.com")}static credential(e,n){return we._fromParams({providerId:he.PROVIDER_ID,signInMethod:he.TWITTER_SIGN_IN_METHOD,oauthToken:e,oauthTokenSecret:n})}static credentialFromResult(e){return he.credentialFromTaggedObject(e)}static credentialFromError(e){return he.credentialFromTaggedObject(e.customData||{})}static credentialFromTaggedObject({_tokenResponse:e}){if(!e)return null;const{oauthAccessToken:n,oauthTokenSecret:s}=e;if(!n||!s)return null;try{return he.credential(n,s)}catch{return null}}}he.TWITTER_SIGN_IN_METHOD="twitter.com";he.PROVIDER_ID="twitter.com";/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Do(t,e){return Ze(t,"POST","/v1/accounts:signUp",Je(t,e))}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Te{constructor(e){this.user=e.user,this.providerId=e.providerId,this._tokenResponse=e._tokenResponse,this.operationType=e.operationType}static async _fromIdTokenResponse(e,n,s,i=!1){const r=await ye._fromIdTokenResponse(e,s,i),a=xn(s);return new Te({user:r,providerId:a,_tokenResponse:s,operationType:n})}static async _forOperation(e,n,s){await e._updateTokensIfNecessary(s,!0);const i=xn(s);return new Te({user:e,providerId:i,_tokenResponse:s,operationType:n})}}function xn(t){return t.providerId?t.providerId:"phoneNumber"in t?"phone":null}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class dt extends me{constructor(e,n,s,i){var r;super(n.code,n.message),this.operationType=s,this.user=i,Object.setPrototypeOf(this,dt.prototype),this.customData={appName:e.name,tenantId:(r=e.tenantId)!==null&&r!==void 0?r:void 0,_serverResponse:n.customData._serverResponse,operationType:s}}static _fromErrorAndOperation(e,n,s,i){return new dt(e,n,s,i)}}function Fs(t,e,n,s){return(e==="reauthenticate"?n._getReauthenticationResolver(t):n._getIdTokenResponse(t)).catch(r=>{throw r.code==="auth/multi-factor-auth-required"?dt._fromErrorAndOperation(t,r,e,s):r})}async function Mo(t,e,n=!1){const s=await Fe(t,e._linkToIdToken(t.auth,await t.getIdToken()),n);return Te._forOperation(t,"link",s)}/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Ws(t,e,n=!1){var s;const{auth:i}=t,r="reauthenticate";try{const a=await Fe(t,Fs(i,r,e,t),n);R(a.idToken,i,"internal-error");const o=xt(a.idToken);R(o,i,"internal-error");const{sub:l}=o;return R(t.uid===l,i,"user-mismatch"),Te._forOperation(t,r,a)}catch(a){throw((s=a)===null||s===void 0?void 0:s.code)==="auth/user-not-found"&&Q(i,"user-mismatch"),a}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function js(t,e,n=!1){const s="signIn",i=await Fs(t,s,e),r=await Te._fromIdTokenResponse(t,s,i);return n||await t._updateCurrentUser(r.user),r}async function Po(t,e){return js(gt(t),e)}async function No(t,e){return Ws(le(t),e)}async function Uo(t,e,n){const s=gt(t),i=await Do(s,{returnSecureToken:!0,email:e,password:n}),r=await Te._fromIdTokenResponse(s,"signIn",i);return await s._updateCurrentUser(r.user),r}function Vo(t,e,n){return Po(le(t),Oe.credential(e,n))}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Qc(t,e){return le(t).setPersistence(e)}function xc(t,e,n,s){return le(t).onAuthStateChanged(e,n,s)}async function Ho(t){return le(t).delete()}const ft="__sak";/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class zs{constructor(e,n){this.storageRetriever=e,this.type=n}_isAvailable(){try{return this.storage?(this.storage.setItem(ft,"1"),this.storage.removeItem(ft),Promise.resolve(!0)):Promise.resolve(!1)}catch{return Promise.resolve(!1)}}_set(e,n){return this.storage.setItem(e,JSON.stringify(n)),Promise.resolve()}_get(e){const n=this.storage.getItem(e);return Promise.resolve(n?JSON.parse(n):null)}_remove(e){return this.storage.removeItem(e),Promise.resolve()}get storage(){return this.storageRetriever()}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Bo(){const t=J();return $t(t)||mt(t)}const Fo=1e3,Wo=10;class Gs extends zs{constructor(){super(()=>window.localStorage,"LOCAL"),this.boundEventHandler=(e,n)=>this.onStorageEvent(e,n),this.listeners={},this.localCache={},this.pollTimer=null,this.safariLocalStorageNotSynced=Bo()&&Eo(),this.fallbackToPolling=Vs(),this._shouldAllowMigration=!0}forAllChangedKeys(e){for(const n of Object.keys(this.listeners)){const s=this.storage.getItem(n),i=this.localCache[n];s!==i&&e(n,i,s)}}onStorageEvent(e,n=!1){if(!e.key){this.forAllChangedKeys((a,o,l)=>{this.notifyListeners(a,l)});return}const s=e.key;if(n?this.detachListener():this.stopPolling(),this.safariLocalStorageNotSynced){const a=this.storage.getItem(s);if(e.newValue!==a)e.newValue!==null?this.storage.setItem(s,e.newValue):this.storage.removeItem(s);else if(this.localCache[s]===e.newValue&&!n)return}const i=()=>{const a=this.storage.getItem(s);!n&&this.localCache[s]===a||this.notifyListeners(s,a)},r=this.storage.getItem(s);Io()&&r!==e.newValue&&e.newValue!==e.oldValue?setTimeout(i,Wo):i()}notifyListeners(e,n){this.localCache[e]=n;const s=this.listeners[e];if(s)for(const i of Array.from(s))i(n&&JSON.parse(n))}startPolling(){this.stopPolling(),this.pollTimer=setInterval(()=>{this.forAllChangedKeys((e,n,s)=>{this.onStorageEvent(new StorageEvent("storage",{key:e,oldValue:n,newValue:s}),!0)})},Fo)}stopPolling(){this.pollTimer&&(clearInterval(this.pollTimer),this.pollTimer=null)}attachListener(){window.addEventListener("storage",this.boundEventHandler)}detachListener(){window.removeEventListener("storage",this.boundEventHandler)}_addListener(e,n){Object.keys(this.listeners).length===0&&(this.fallbackToPolling?this.startPolling():this.attachListener()),this.listeners[e]||(this.listeners[e]=new Set,this.localCache[e]=this.storage.getItem(e)),this.listeners[e].add(n)}_removeListener(e,n){this.listeners[e]&&(this.listeners[e].delete(n),this.listeners[e].size===0&&delete this.listeners[e]),Object.keys(this.listeners).length===0&&(this.detachListener(),this.stopPolling())}async _set(e,n){await super._set(e,n),this.localCache[e]=JSON.stringify(n)}async _get(e){const n=await super._get(e);return this.localCache[e]=JSON.stringify(n),n}async _remove(e){await super._remove(e),delete this.localCache[e]}}Gs.type="LOCAL";const jo=Gs;/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class qs extends zs{constructor(){super(()=>window.sessionStorage,"SESSION")}_addListener(e,n){}_removeListener(e,n){}}qs.type="SESSION";const Ks=qs;/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function zo(t){return Promise.all(t.map(async e=>{try{const n=await e;return{fulfilled:!0,value:n}}catch(n){return{fulfilled:!1,reason:n}}}))}/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class pt{constructor(e){this.eventTarget=e,this.handlersMap={},this.boundEventHandler=this.handleEvent.bind(this)}static _getInstance(e){const n=this.receivers.find(i=>i.isListeningto(e));if(n)return n;const s=new pt(e);return this.receivers.push(s),s}isListeningto(e){return this.eventTarget===e}async handleEvent(e){const n=e,{eventId:s,eventType:i,data:r}=n.data,a=this.handlersMap[i];if(!(a!=null&&a.size))return;n.ports[0].postMessage({status:"ack",eventId:s,eventType:i});const o=Array.from(a).map(async c=>c(n.origin,r)),l=await zo(o);n.ports[0].postMessage({status:"done",eventId:s,eventType:i,response:l})}_subscribe(e,n){Object.keys(this.handlersMap).length===0&&this.eventTarget.addEventListener("message",this.boundEventHandler),this.handlersMap[e]||(this.handlersMap[e]=new Set),this.handlersMap[e].add(n)}_unsubscribe(e,n){this.handlersMap[e]&&n&&this.handlersMap[e].delete(n),(!n||this.handlersMap[e].size===0)&&delete this.handlersMap[e],Object.keys(this.handlersMap).length===0&&this.eventTarget.removeEventListener("message",this.boundEventHandler)}}pt.receivers=[];/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function nn(t="",e=10){let n="";for(let s=0;s<e;s++)n+=Math.floor(Math.random()*10);return t+n}/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Go{constructor(e){this.target=e,this.handlers=new Set}removeMessageHandler(e){e.messageChannel&&(e.messageChannel.port1.removeEventListener("message",e.onMessage),e.messageChannel.port1.close()),this.handlers.delete(e)}async _send(e,n,s=50){const i=typeof MessageChannel<"u"?new MessageChannel:null;if(!i)throw new Error("connection_unavailable");let r,a;return new Promise((o,l)=>{const c=nn("",20);i.port1.start();const u=setTimeout(()=>{l(new Error("unsupported_event"))},s);a={messageChannel:i,onMessage(f){const m=f;if(m.data.eventId===c)switch(m.data.status){case"ack":clearTimeout(u),r=setTimeout(()=>{l(new Error("timeout"))},3e3);break;case"done":clearTimeout(r),o(m.data.response);break;default:clearTimeout(u),clearTimeout(r),l(new Error("invalid_response"));break}}},this.handlers.add(a),i.port1.addEventListener("message",a.onMessage),this.target.postMessage({eventType:e,eventId:c,data:n},[i.port2])}).finally(()=>{a&&this.removeMessageHandler(a)})}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function ne(){return window}function qo(t){ne().location.href=t}/**
 * @license
 * Copyright 2020 Google LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Js(){return typeof ne().WorkerGlobalScope<"u"&&typeof ne().importScripts=="function"}async function Ko(){if(!(navigator!=null&&navigator.serviceWorker))return null;try{return(await navigator.serviceWorker.ready).active}catch{return null}}function Jo(){var t;return((t=navigator==null?void 0:navigator.serviceWorker)===null||t===void 0?void 0:t.controller)||null}function Xo(){return Js()?self:null}/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Xs="firebaseLocalStorageDb",Zo=1,ht="firebaseLocalStorage",Zs="fbase_key";class Qe{constructor(e){this.request=e}toPromise(){return new Promise((e,n)=>{this.request.addEventListener("success",()=>{e(this.request.result)}),this.request.addEventListener("error",()=>{n(this.request.error)})})}}function vt(t,e){return t.transaction([ht],e?"readwrite":"readonly").objectStore(ht)}function Yo(){const t=indexedDB.deleteDatabase(Xs);return new Qe(t).toPromise()}function Bt(){const t=indexedDB.open(Xs,Zo);return new Promise((e,n)=>{t.addEventListener("error",()=>{n(t.error)}),t.addEventListener("upgradeneeded",()=>{const s=t.result;try{s.createObjectStore(ht,{keyPath:Zs})}catch(i){n(i)}}),t.addEventListener("success",async()=>{const s=t.result;s.objectStoreNames.contains(ht)?e(s):(s.close(),await Yo(),e(await Bt()))})})}async function $n(t,e,n){const s=vt(t,!0).put({[Zs]:e,value:n});return new Qe(s).toPromise()}async function Qo(t,e){const n=vt(t,!1).get(e),s=await new Qe(n).toPromise();return s===void 0?null:s.value}function es(t,e){const n=vt(t,!0).delete(e);return new Qe(n).toPromise()}const xo=800,$o=3;class Ys{constructor(){this.type="LOCAL",this._shouldAllowMigration=!0,this.listeners={},this.localCache={},this.pollTimer=null,this.pendingWrites=0,this.receiver=null,this.sender=null,this.serviceWorkerReceiverAvailable=!1,this.activeServiceWorker=null,this._workerInitializationPromise=this.initializeServiceWorkerMessaging().then(()=>{},()=>{})}async _openDb(){return this.db?this.db:(this.db=await Bt(),this.db)}async _withRetries(e){let n=0;for(;;)try{const s=await this._openDb();return await e(s)}catch(s){if(n++>$o)throw s;this.db&&(this.db.close(),this.db=void 0)}}async initializeServiceWorkerMessaging(){return Js()?this.initializeReceiver():this.initializeSender()}async initializeReceiver(){this.receiver=pt._getInstance(Xo()),this.receiver._subscribe("keyChanged",async(e,n)=>({keyProcessed:(await this._poll()).includes(n.key)})),this.receiver._subscribe("ping",async(e,n)=>["keyChanged"])}async initializeSender(){var e,n;if(this.activeServiceWorker=await Ko(),!this.activeServiceWorker)return;this.sender=new Go(this.activeServiceWorker);const s=await this.sender._send("ping",{},800);!s||((e=s[0])===null||e===void 0?void 0:e.fulfilled)&&((n=s[0])===null||n===void 0?void 0:n.value.includes("keyChanged"))&&(this.serviceWorkerReceiverAvailable=!0)}async notifyServiceWorker(e){if(!(!this.sender||!this.activeServiceWorker||Jo()!==this.activeServiceWorker))try{await this.sender._send("keyChanged",{key:e},this.serviceWorkerReceiverAvailable?800:50)}catch{}}async _isAvailable(){try{if(!indexedDB)return!1;const e=await Bt();return await $n(e,ft,"1"),await es(e,ft),!0}catch{}return!1}async _withPendingWrite(e){this.pendingWrites++;try{await e()}finally{this.pendingWrites--}}async _set(e,n){return this._withPendingWrite(async()=>(await this._withRetries(s=>$n(s,e,n)),this.localCache[e]=n,this.notifyServiceWorker(e)))}async _get(e){const n=await this._withRetries(s=>Qo(s,e));return this.localCache[e]=n,n}async _remove(e){return this._withPendingWrite(async()=>(await this._withRetries(n=>es(n,e)),delete this.localCache[e],this.notifyServiceWorker(e)))}async _poll(){const e=await this._withRetries(i=>{const r=vt(i,!1).getAll();return new Qe(r).toPromise()});if(!e)return[];if(this.pendingWrites!==0)return[];const n=[],s=new Set;for(const{fbase_key:i,value:r}of e)s.add(i),JSON.stringify(this.localCache[i])!==JSON.stringify(r)&&(this.notifyListeners(i,r),n.push(i));for(const i of Object.keys(this.localCache))this.localCache[i]&&!s.has(i)&&(this.notifyListeners(i,null),n.push(i));return n}notifyListeners(e,n){this.localCache[e]=n;const s=this.listeners[e];if(s)for(const i of Array.from(s))i(n)}startPolling(){this.stopPolling(),this.pollTimer=setInterval(async()=>this._poll(),xo)}stopPolling(){this.pollTimer&&(clearInterval(this.pollTimer),this.pollTimer=null)}_addListener(e,n){Object.keys(this.listeners).length===0&&this.startPolling(),this.listeners[e]||(this.listeners[e]=new Set,this._get(e)),this.listeners[e].add(n)}_removeListener(e,n){this.listeners[e]&&(this.listeners[e].delete(n),this.listeners[e].size===0&&delete this.listeners[e]),Object.keys(this.listeners).length===0&&this.stopPolling()}}Ys.type="LOCAL";const el=Ys;/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function tl(){var t,e;return(e=(t=document.getElementsByTagName("head"))===null||t===void 0?void 0:t[0])!==null&&e!==void 0?e:document}function nl(t){return new Promise((e,n)=>{const s=document.createElement("script");s.setAttribute("src",t),s.onload=e,s.onerror=i=>{const r=te("internal-error");r.customData=i,n(r)},s.type="text/javascript",s.charset="UTF-8",tl().appendChild(s)})}function sl(t){return`__${t}${Math.floor(Math.random()*1e6)}`}new Ke(3e4,6e4);/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function il(t,e){return e?ie(e):(R(t._popupRedirectResolver,t,"argument-error"),t._popupRedirectResolver)}/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class sn extends en{constructor(e){super("custom","custom"),this.params=e}_getIdTokenResponse(e){return Se(e,this._buildIdpRequest())}_linkToIdToken(e,n){return Se(e,this._buildIdpRequest(n))}_getReauthenticationResolver(e){return Se(e,this._buildIdpRequest())}_buildIdpRequest(e){const n={requestUri:this.params.requestUri,sessionId:this.params.sessionId,postBody:this.params.postBody,tenantId:this.params.tenantId,pendingToken:this.params.pendingToken,returnSecureToken:!0,returnIdpCredential:!0};return e&&(n.idToken=e),n}}function rl(t){return js(t.auth,new sn(t),t.bypassAuthState)}function al(t){const{auth:e,user:n}=t;return R(n,e,"internal-error"),Ws(n,new sn(t),t.bypassAuthState)}async function ol(t){const{auth:e,user:n}=t;return R(n,e,"internal-error"),Mo(n,new sn(t),t.bypassAuthState)}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Qs{constructor(e,n,s,i,r=!1){this.auth=e,this.resolver=s,this.user=i,this.bypassAuthState=r,this.pendingPromise=null,this.eventManager=null,this.filter=Array.isArray(n)?n:[n]}execute(){return new Promise(async(e,n)=>{this.pendingPromise={resolve:e,reject:n};try{this.eventManager=await this.resolver._initialize(this.auth),await this.onExecution(),this.eventManager.registerConsumer(this)}catch(s){this.reject(s)}})}async onAuthEvent(e){const{urlResponse:n,sessionId:s,postBody:i,tenantId:r,error:a,type:o}=e;if(a){this.reject(a);return}const l={auth:this.auth,requestUri:n,sessionId:s,tenantId:r||void 0,postBody:i||void 0,user:this.user,bypassAuthState:this.bypassAuthState};try{this.resolve(await this.getIdpTask(o)(l))}catch(c){this.reject(c)}}onError(e){this.reject(e)}getIdpTask(e){switch(e){case"signInViaPopup":case"signInViaRedirect":return rl;case"linkViaPopup":case"linkViaRedirect":return ol;case"reauthViaPopup":case"reauthViaRedirect":return al;default:Q(this.auth,"internal-error")}}resolve(e){oe(this.pendingPromise,"Pending promise was never set"),this.pendingPromise.resolve(e),this.unregisterAndCleanUp()}reject(e){oe(this.pendingPromise,"Pending promise was never set"),this.pendingPromise.reject(e),this.unregisterAndCleanUp()}unregisterAndCleanUp(){this.eventManager&&this.eventManager.unregisterConsumer(this),this.pendingPromise=null,this.cleanUp()}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const ll=new Ke(2e3,1e4);class ke extends Qs{constructor(e,n,s,i,r){super(e,n,i,r),this.provider=s,this.authWindow=null,this.pollId=null,ke.currentPopupAction&&ke.currentPopupAction.cancel(),ke.currentPopupAction=this}async executeNotNull(){const e=await this.execute();return R(e,this.auth,"internal-error"),e}async onExecution(){oe(this.filter.length===1,"Popup operations only handle one event");const e=nn();this.authWindow=await this.resolver._openPopup(this.auth,this.provider,this.filter[0],e),this.authWindow.associatedEvent=e,this.resolver._originValidation(this.auth).catch(n=>{this.reject(n)}),this.resolver._isIframeWebStorageSupported(this.auth,n=>{n||this.reject(te(this.auth,"web-storage-unsupported"))}),this.pollUserCancellation()}get eventId(){var e;return((e=this.authWindow)===null||e===void 0?void 0:e.associatedEvent)||null}cancel(){this.reject(te(this.auth,"cancelled-popup-request"))}cleanUp(){this.authWindow&&this.authWindow.close(),this.pollId&&window.clearTimeout(this.pollId),this.authWindow=null,this.pollId=null,ke.currentPopupAction=null}pollUserCancellation(){const e=()=>{var n,s;if(!((s=(n=this.authWindow)===null||n===void 0?void 0:n.window)===null||s===void 0)&&s.closed){this.pollId=window.setTimeout(()=>{this.pollId=null,this.reject(te(this.auth,"popup-closed-by-user"))},2e3);return}this.pollId=window.setTimeout(e,ll.get())};e()}}ke.currentPopupAction=null;/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const cl="pendingRedirect",it=new Map;class ul extends Qs{constructor(e,n,s=!1){super(e,["signInViaRedirect","linkViaRedirect","reauthViaRedirect","unknown"],n,void 0,s),this.eventId=null}async execute(){let e=it.get(this.auth._key());if(!e){try{const s=await dl(this.resolver,this.auth)?await super.execute():null;e=()=>Promise.resolve(s)}catch(n){e=()=>Promise.reject(n)}it.set(this.auth._key(),e)}return this.bypassAuthState||it.set(this.auth._key(),()=>Promise.resolve(null)),e()}async onAuthEvent(e){if(e.type==="signInViaRedirect")return super.onAuthEvent(e);if(e.type==="unknown"){this.resolve(null);return}if(e.eventId){const n=await this.auth._redirectUserForId(e.eventId);if(n)return this.user=n,super.onAuthEvent(e);this.resolve(null)}}async onExecution(){}cleanUp(){}}async function dl(t,e){const n=_l(e),s=hl(t);if(!await s._isAvailable())return!1;const i=await s._get(n)==="true";return await s._remove(n),i}function fl(t,e){it.set(t._key(),e)}function hl(t){return ie(t._redirectPersistence)}function _l(t){return st(cl,t.config.apiKey,t.name)}async function ml(t,e,n=!1){const s=gt(t),i=il(s,e),a=await new ul(s,i,n).execute();return a&&!n&&(delete a.user._redirectEventId,await s._persistUserIfCurrent(a.user),await s._setRedirectUser(null,e)),a}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const gl=10*60*1e3;class pl{constructor(e){this.auth=e,this.cachedEventUids=new Set,this.consumers=new Set,this.queuedRedirectEvent=null,this.hasHandledPotentialRedirect=!1,this.lastProcessedEventTime=Date.now()}registerConsumer(e){this.consumers.add(e),this.queuedRedirectEvent&&this.isEventForConsumer(this.queuedRedirectEvent,e)&&(this.sendToConsumer(this.queuedRedirectEvent,e),this.saveEventToCache(this.queuedRedirectEvent),this.queuedRedirectEvent=null)}unregisterConsumer(e){this.consumers.delete(e)}onEvent(e){if(this.hasEventBeenHandled(e))return!1;let n=!1;return this.consumers.forEach(s=>{this.isEventForConsumer(e,s)&&(n=!0,this.sendToConsumer(e,s),this.saveEventToCache(e))}),this.hasHandledPotentialRedirect||!vl(e)||(this.hasHandledPotentialRedirect=!0,n||(this.queuedRedirectEvent=e,n=!0)),n}sendToConsumer(e,n){var s;if(e.error&&!xs(e)){const i=((s=e.error.code)===null||s===void 0?void 0:s.split("auth/")[1])||"internal-error";n.onError(te(this.auth,i))}else n.onAuthEvent(e)}isEventForConsumer(e,n){const s=n.eventId===null||!!e.eventId&&e.eventId===n.eventId;return n.filter.includes(e.type)&&s}hasEventBeenHandled(e){return Date.now()-this.lastProcessedEventTime>=gl&&this.cachedEventUids.clear(),this.cachedEventUids.has(ts(e))}saveEventToCache(e){this.cachedEventUids.add(ts(e)),this.lastProcessedEventTime=Date.now()}}function ts(t){return[t.type,t.eventId,t.sessionId,t.tenantId].filter(e=>e).join("-")}function xs({type:t,error:e}){return t==="unknown"&&(e==null?void 0:e.code)==="auth/no-auth-event"}function vl(t){switch(t.type){case"signInViaRedirect":case"linkViaRedirect":case"reauthViaRedirect":return!0;case"unknown":return xs(t);default:return!1}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function yl(t,e={}){return Xe(t,"GET","/v1/projects",e)}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const bl=/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/,Il=/^https?/;async function El(t){if(t.config.emulator)return;const{authorizedDomains:e}=await yl(t);for(const n of e)try{if(wl(n))return}catch{}Q(t,"unauthorized-domain")}function wl(t){const e=Ht(),{protocol:n,hostname:s}=new URL(e);if(t.startsWith("chrome-extension://")){const a=new URL(t);return a.hostname===""&&s===""?n==="chrome-extension:"&&t.replace("chrome-extension://","")===e.replace("chrome-extension://",""):n==="chrome-extension:"&&a.hostname===s}if(!Il.test(n))return!1;if(bl.test(t))return s===t;const i=t.replace(/\./g,"\\.");return new RegExp("^(.+\\."+i+"|"+i+")$","i").test(s)}/**
 * @license
 * Copyright 2020 Google LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Tl=new Ke(3e4,6e4);function ns(){const t=ne().___jsl;if(t!=null&&t.H){for(const e of Object.keys(t.H))if(t.H[e].r=t.H[e].r||[],t.H[e].L=t.H[e].L||[],t.H[e].r=[...t.H[e].L],t.CP)for(let n=0;n<t.CP.length;n++)t.CP[n]=null}}function kl(t){return new Promise((e,n)=>{var s,i,r;function a(){ns(),gapi.load("gapi.iframes",{callback:()=>{e(gapi.iframes.getContext())},ontimeout:()=>{ns(),n(te(t,"network-request-failed"))},timeout:Tl.get()})}if(!((i=(s=ne().gapi)===null||s===void 0?void 0:s.iframes)===null||i===void 0)&&i.Iframe)e(gapi.iframes.getContext());else if(!((r=ne().gapi)===null||r===void 0)&&r.load)a();else{const o=sl("iframefcb");return ne()[o]=()=>{gapi.load?a():n(te(t,"network-request-failed"))},nl(`https://apis.google.com/js/api.js?onload=${o}`).catch(l=>n(l))}}).catch(e=>{throw rt=null,e})}let rt=null;function Cl(t){return rt=rt||kl(t),rt}/**
 * @license
 * Copyright 2020 Google LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Sl=new Ke(5e3,15e3),Al="__/auth/iframe",Ll="emulator/auth/iframe",Rl={style:{position:"absolute",top:"-100px",width:"1px",height:"1px"},"aria-hidden":"true",tabindex:"-1"},Ol=new Map([["identitytoolkit.googleapis.com","p"],["staging-identitytoolkit.sandbox.googleapis.com","s"],["test-identitytoolkit.sandbox.googleapis.com","t"]]);function Dl(t){const e=t.config;R(e.authDomain,t,"auth-domain-config-required");const n=e.emulator?Qt(e,Ll):`https://${t.config.authDomain}/${Al}`,s={apiKey:e.apiKey,appName:t.name,v:_t},i=Ol.get(t.config.apiHost);i&&(s.eid=i);const r=t._getFrameworks();return r.length&&(s.fw=r.join(",")),`${n}?${qe(s).slice(1)}`}async function Ml(t){const e=await Cl(t),n=ne().gapi;return R(n,t,"internal-error"),e.open({where:document.body,url:Dl(t),messageHandlersFilter:n.iframes.CROSS_ORIGIN_IFRAMES_FILTER,attributes:Rl,dontclear:!0},s=>new Promise(async(i,r)=>{await s.restyle({setHideOnLeave:!1});const a=te(t,"network-request-failed"),o=ne().setTimeout(()=>{r(a)},Sl.get());function l(){ne().clearTimeout(o),i(s)}s.ping(l).then(l,()=>{r(a)})}))}/**
 * @license
 * Copyright 2020 Google LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Pl={location:"yes",resizable:"yes",statusbar:"yes",toolbar:"no"},Nl=500,Ul=600,Vl="_blank",Hl="http://localhost";class ss{constructor(e){this.window=e,this.associatedEvent=null}close(){if(this.window)try{this.window.close()}catch{}}}function Bl(t,e,n,s=Nl,i=Ul){const r=Math.max((window.screen.availHeight-i)/2,0).toString(),a=Math.max((window.screen.availWidth-s)/2,0).toString();let o="";const l=Object.assign(Object.assign({},Pl),{width:s.toString(),height:i.toString(),top:r,left:a}),c=J().toLowerCase();n&&(o=Ds(c)?Vl:n),Os(c)&&(e=e||Hl,l.scrollbars="yes");const u=Object.entries(l).reduce((m,[d,h])=>`${m}${d}=${h},`,"");if(bo(c)&&o!=="_self")return Fl(e||"",o),new ss(null);const f=window.open(e||"",o,u);R(f,t,"popup-blocked");try{f.focus()}catch{}return new ss(f)}function Fl(t,e){const n=document.createElement("a");n.href=t,n.target=e;const s=document.createEvent("MouseEvent");s.initMouseEvent("click",!0,!0,window,1,0,0,0,0,!1,!1,!1,!1,1,null),n.dispatchEvent(s)}/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Wl="__/auth/handler",jl="emulator/auth/handler";function is(t,e,n,s,i,r){R(t.config.authDomain,t,"auth-domain-config-required"),R(t.config.apiKey,t,"invalid-api-key");const a={apiKey:t.config.apiKey,appName:t.name,authType:n,redirectUrl:s,v:_t,eventId:i};if(e instanceof Bs){e.setDefaultLanguage(t.languageCode),a.providerId=e.providerId||"",zr(e.getCustomParameters())||(a.customParameters=JSON.stringify(e.getCustomParameters()));for(const[l,c]of Object.entries(r||{}))a[l]=c}if(e instanceof Ye){const l=e.getScopes().filter(c=>c!=="");l.length>0&&(a.scopes=l.join(","))}t.tenantId&&(a.tid=t.tenantId);const o=a;for(const l of Object.keys(o))o[l]===void 0&&delete o[l];return`${zl(t)}?${qe(o).slice(1)}`}function zl({config:t}){return t.emulator?Qt(t,jl):`https://${t.authDomain}/${Wl}`}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Lt="webStorageSupport";class Gl{constructor(){this.eventManagers={},this.iframes={},this.originValidationPromises={},this._redirectPersistence=Ks,this._completeRedirectFn=ml,this._overrideRedirectResult=fl}async _openPopup(e,n,s,i){var r;oe((r=this.eventManagers[e._key()])===null||r===void 0?void 0:r.manager,"_initialize() not called before _openPopup()");const a=is(e,n,s,Ht(),i);return Bl(e,a,nn())}async _openRedirect(e,n,s,i){return await this._originValidation(e),qo(is(e,n,s,Ht(),i)),new Promise(()=>{})}_initialize(e){const n=e._key();if(this.eventManagers[n]){const{manager:i,promise:r}=this.eventManagers[n];return i?Promise.resolve(i):(oe(r,"If manager is not set, promise should be"),r)}const s=this.initAndGetManager(e);return this.eventManagers[n]={promise:s},s.catch(()=>{delete this.eventManagers[n]}),s}async initAndGetManager(e){const n=await Ml(e),s=new pl(e);return n.register("authEvent",i=>(R(i==null?void 0:i.authEvent,e,"invalid-auth-event"),{status:s.onEvent(i.authEvent)?"ACK":"ERROR"}),gapi.iframes.CROSS_ORIGIN_IFRAMES_FILTER),this.eventManagers[e._key()]={manager:s},this.iframes[e._key()]=n,s}_isIframeWebStorageSupported(e,n){this.iframes[e._key()].send(Lt,{type:Lt},i=>{var r;const a=(r=i==null?void 0:i[0])===null||r===void 0?void 0:r[Lt];a!==void 0&&n(!!a),Q(e,"internal-error")},gapi.iframes.CROSS_ORIGIN_IFRAMES_FILTER)}_originValidation(e){const n=e._key();return this.originValidationPromises[n]||(this.originValidationPromises[n]=El(e)),this.originValidationPromises[n]}get _shouldInitProactively(){return Vs()||$t()||mt()}}const ql=Gl;var rs="@firebase/auth",as="0.20.6";/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Kl{constructor(e){this.auth=e,this.internalListeners=new Map}getUid(){var e;return this.assertAuthConfigured(),((e=this.auth.currentUser)===null||e===void 0?void 0:e.uid)||null}async getToken(e){return this.assertAuthConfigured(),await this.auth._initializationPromise,this.auth.currentUser?{accessToken:await this.auth.currentUser.getIdToken(e)}:null}addAuthTokenListener(e){if(this.assertAuthConfigured(),this.internalListeners.has(e))return;const n=this.auth.onIdTokenChanged(s=>{var i;e(((i=s)===null||i===void 0?void 0:i.stsTokenManager.accessToken)||null)});this.internalListeners.set(e,n),this.updateProactiveRefresh()}removeAuthTokenListener(e){this.assertAuthConfigured();const n=this.internalListeners.get(e);!n||(this.internalListeners.delete(e),n(),this.updateProactiveRefresh())}assertAuthConfigured(){R(this.auth._initializationPromise,"dependent-sdk-initialized-before-auth")}updateProactiveRefresh(){this.internalListeners.size>0?this.auth._startProactiveRefresh():this.auth._stopProactiveRefresh()}}/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Jl(t){switch(t){case"Node":return"node";case"ReactNative":return"rn";case"Worker":return"webworker";case"Cordova":return"cordova";default:return}}function Xl(t){He(new Le("auth",(e,{options:n})=>{const s=e.getProvider("app").getImmediate(),i=e.getProvider("heartbeat"),{apiKey:r,authDomain:a}=s.options;return((o,l)=>{R(r&&!r.includes(":"),"invalid-api-key",{appName:o.name}),R(!(a!=null&&a.includes(":")),"argument-error",{appName:o.name});const c={apiKey:r,authDomain:a,clientPlatform:t,apiHost:"identitytoolkit.googleapis.com",tokenApiHost:"securetoken.googleapis.com",apiScheme:"https",sdkClientVersion:Hs(t)},u=new To(o,l,c);return no(u,n),u})(s,i)},"PUBLIC").setInstantiationMode("EXPLICIT").setInstanceCreatedCallback((e,n,s)=>{e.getProvider("auth-internal").initialize()})),He(new Le("auth-internal",e=>{const n=gt(e.getProvider("auth").getImmediate());return(s=>new Kl(s))(n)},"PRIVATE").setInstantiationMode("EXPLICIT")),Ue(rs,as,Jl(t)),Ue(rs,as,"esm2017")}/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function $c(t=za()){const e=Is(t,"auth");return e.isInitialized()?e.getImmediate():to(t,{popupRedirectResolver:ql,persistence:[el,jo,Ks]})}Xl("Browser");const Zl={"2miners":{ETH:{account:"https://eth.2miners.com/account/<$>",api:"https://eth.2miners.com/api/accounts/<$>",modifier:1e9},XMR:{account:"https://xmr.2miners.com/account/<$>",api:"https://xmr.2miners.com/api/accounts/<$>",modifier:1e12},RVN:{account:"https://rvn.2miners.com/account/<$>",api:"https://rvn.2miners.com/api/accounts/<$>",modifier:1e8},CLO:{account:"https://clo.2miners.com/account/<$>",api:"https://clo.2miners.com/api/accounts/<$>",modifier:1e9}},ethermine:{ETH:{account:"https://ethermine.org/miners/<$>/dashboard",api:"https://api.ethermine.org/miners/<$>/dashboard",modifier:1},RVN:{account:"https://api-ravencoin.flypool.org/miners/<$>/dashboard",api:"https://api-ravencoin.flypool.org/miners/<$>/dashboard",modifier:1}},minexmr:{XMR:{account:"https://minexmr.com/dashboard?address=/<$>",api:"https://minexmr.com/api/main/user/stats?address=/<$>",modifier:1}}},Yl={cfg:{apiKey:"AIzaSyD03o8fUeJPDqwtN8vOIVygJx1arXqZtT0",authDomain:"poolance-crypto-monitor.firebaseapp.com",projectId:"poolance-crypto-monitor",storageBucket:"poolance-crypto-monitor.appspot.com",messagingSenderId:"711065226977",appId:"1:711065226977:web:afb734d8c3eb036aca71a0"}},os={pools:Zl,firebase:Yl},xe=["https://europe-west3-poolance-crypto-monitor.cloudfunctions.net","http://localhost:5839"][0];async function Ql(t){const e={method:"POST",headers:{"Content-Type":"application/json",Token:`Bearer ${t}`}};await fetch(`${xe}/account`,e).then(n=>{if(!n.ok)throw new Error(n.statusText)})}async function xl(t){const e={method:"DELETE",headers:{"Content-Type":"application/json",Token:`Bearer ${t}`}};await fetch(`${xe}/account`,e).then(n=>{if(!n.ok)throw new Error(n.statusText)})}async function $l(t){let e=[];Et.set(null);const n={headers:{"Content-Type":"application/json",Token:`Bearer ${t}`}};await fetch(`${xe}/assets`,n).then(s=>{if(!s.ok)throw new Error(s.statusText);return s.json()}).then(s=>{for(var i=0;i<s.length;i++){var r=s[i].name;e.push({pool:r,coins:[]});for(var a="",o=0;o<s[i].wallets.length;o++){var l=s[i].wallets[o].asset,c=s[i].wallets[o].wallet;a=os.pools[r][l].account.split("<$>");var u=`${a[0]}${c}${a[1]}`;a=os.pools[r][l].api.split("<$>");var f=`${a[0]}${c}${a[1]}`;e[i].coins.push({coin:l,wallet:c,account:u,api:f})}}}).catch(()=>{Et.set([])}),Et.set(e)}async function ec(t,e){const n={method:"PUT",headers:{"Content-Type":"application/json",Token:`Bearer ${t}`},body:JSON.stringify(e)};await fetch(`${xe}/assets/pools`,n).then(s=>{if(!s.ok)throw new Error(s.statusText);return s.json()}).catch(s=>{console.error(s)})}async function tc(t,e,n){const s={method:"PUT",headers:{"Content-Type":"application/json",Token:`Bearer ${t}`},body:JSON.stringify(n)};await fetch(`${xe}/assets?pool=${e}`,s).then(i=>{if(!i.ok)throw new Error(i.statusText);return i.json()}).catch(i=>{console.error(i)})}const $s={logup:Ql,remove:xl,loadData:$l,updatePools:ec,updatePoolCoins:tc};let Re;yr.subscribe(t=>{Re=t});async function nc(t=""){await navigator.clipboard.writeText(t)}async function sc(t,e){let n=null;return await Uo(Re,t,e).then(async s=>{n=s.user,await $s.logup(await Re.currentUser.getIdToken())}).catch(s=>{throw console.error(s),s}),n}async function ic(t,e){let n=null;return await Vo(Re,t,e).then(s=>{n=s.user}).catch(s=>{throw console.error(s),s}),n}async function rc(t,e){let n=null;return await No(Re,t).then(s=>{n=s.user}).catch(s=>{throw console.error(s),s}),n}async function ac(t){const e=await Re.currentUser.getIdToken();await Ho(t).then(async()=>{await $s.remove(e)}).catch(n=>{throw console.error(n),n})}const eu={copyClip:nc,logup:sc,login:ic,reauth:rc,remove:ac};export{Tc as $,Et as A,lc as B,os as C,hc as D,$s as E,_c as F,Mc as G,mc as H,Ei as I,or as J,Ue as K,dc as L,Yc as M,$c as N,Qc as O,xc as P,Yl as Q,ds as R,fs as S,Si as T,jo as U,yc as V,Dc as W,qc as X,Xc as Y,kc as Z,Cc as _,yr as a,Gc as a0,Oc as a1,Wc as a2,jc as a3,zc as a4,Vc as a5,pc as a6,bc as b,eu as c,Ac as d,Sc as e,Yi as f,Zc as g,Kc as h,uc as i,Lc as j,Nc as k,Ic as l,Pc as m,Jc as n,Rc as o,wc as p,Fc as q,Ec as r,vc as s,gc as t,Pi as u,fc as v,cc as w,Bc as x,Uc as y,Hc as z};
