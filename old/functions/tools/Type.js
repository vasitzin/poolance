module.exports.Type = class {
  /**
   * @param {Object} object 
   * @param {Array<String>} reqKeys 
   * @param {Array<String>} reqTypes 
   */
  integrity(object, reqKeys, reqTypes) {
    var keys = Object.keys(object);
    var values = Object.values(object);
    for (var i = 0; i < reqKeys.length; i++) {
      if (!keys.includes(reqKeys[i])) // <- check for undefined
        throw new Error(`[W7A4PS] Requirement ${reqKeys[i]} not satisfied.`);
      else if (values[keys.indexOf(reqKeys[i])] === null) // <- check for null
        throw new Error(`[LF9WRZ] Requirement ${reqKeys[i]} not satisfied with null.`);
      else if (typeof values[keys.indexOf(reqKeys[i])] !== reqTypes[i]) // <- check for type
        throw new Error(`[PFN3S4] Requirement ${reqKeys[i]} invalid format, ` +
          `given ${typeof values[keys.indexOf(reqKeys[i])]} needs ${reqTypes[i]}.`);
    }
  }
}