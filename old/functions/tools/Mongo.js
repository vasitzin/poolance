const env = require('../env.json');
const mongo = require('mongodb');

module.exports.Mongo = class {
  constructor() {
    this.uri = env.mongo.uri;
    this.client = new mongo.MongoClient(this.uri);
    this.db = this.client.db();
    this.pools = this.db.collection("pools");
  }

  /**
   * Get a new ObjectId or one based on given string
   * @param {?String} val the value for the objectid
   * @returns {mongo.ObjectId} an ObjectId
   */
  objId(val) {
    var id;
    val ? id = new mongo.ObjectId(val) : id = new mongo.ObjectId();
    return id;
  }

  /**
   * Executes functions while auto connects and closes the client
   * @param {Function} next the function to be executed
   */
  async exec(next) {
    await this.client.connect()
      .then(async () => { await next(); })
      .catch(err => { throw err; })
      .finally(() => { this.client.close(); });
  }

  /**
   * Executes functions while auto connects the client
   * @param {Function} next the function to be executed
   */
  async conn(next) {
    await this.client.connect()
      .then(async () => { await next(); })
      .catch(err => { throw err; });
  }

  /**
   * Executes functions while auto closes the client
   * @param {Function} next the function to be executed
   */
  async dcon(next) {
    await next()
      .catch(err => { throw err; })
      .finally(() => { this.client.close(); });
  }
}