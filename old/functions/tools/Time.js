const months = new Map([
  [0, "January"],
  [1, "February"],
  [2, "March"],
  [3, "April"],
  [4, "May"],
  [5, "June"],
  [6, "July"],
  [7, "August"],
  [8, "September"],
  [9, "October"],
  [10, "November"],
  [11, "December"],
]);

module.exports.Time = class Time {
  /**
   * Get a date object at the start of current day.
   */
  getDay() {
    return new Date(
      new Date().getUTCFullYear(), new Date().getUTCMonth(), new Date().getUTCDate()
    );
  }

  /**
   * Get a date object at the start of given month.
   * @param {Number} date Date.valueOf
   */
  getThisMonth(date) {
    const obj = new Date(date);
    return new Date(
      obj.getUTCFullYear(), obj.getUTCMonth()
    );
  }

  /**
   * Get a date object at the start of current month.
   */
  getMonth() {
    return new Date(
      new Date().getUTCFullYear(), new Date().getUTCMonth()
    );
  }

  /**
   * Get a date object at the start of previous month.
   */
  getPrevMonth() {
    return new Date(
      new Date().getUTCFullYear(), new Date().getUTCMonth() - 1
    );
  }

  /**
   * Get a date object at the start of next month.
   */
  getNextMonth() {
    return new Date(
      new Date().getUTCFullYear(), new Date().getUTCMonth() + 1
    );
  }

  /**
   * Get a string of given month or by default current month.
   * @param {Number} date Date.valueOf
   */
  getStrMonth(date = new Date().valueOf()) {
    return months.get(new Date(date).getUTCMonth());
  }

  /**
   * Get a string of given month or by default current month with the year.
   * @param {Number} date Date.valueOf
   */
  getDescMonth(date = new Date().valueOf()) {
    var obj = new Date(date);
    return `${months.get(obj.getUTCMonth())} ${obj.getUTCFullYear()}`;
  }
}