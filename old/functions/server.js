const env = require('./env.json');
const admin = require('firebase-admin');
admin.initializeApp({
  ...env.firebase.cfg,
  credential: admin.credential.cert(env.firebase.srv)
});
const express = require('express');
const account = require('./account');
const assets = require('./assets');

module.exports = express()
  .use(express.json())
  .use('/account', account)
  .use('/assets', assets)
  .listen(5839);