const envFn = require('./env.json');
const express = require('express');
const cors = require('cors');
const { Mongo } = require('./tools');
const { Authentication } = require('./Authentication');
const mongo = new Mongo();
const auth = new Authentication();

const origin = [
  "https://poolance-crypto-monitor.web.app",
  "http://localhost:5173"][envFn.server];

module.exports = express().use(cors({ origin }), auth.token)
  .get("", async (req, res) => {
    let data = [];
    await mongo.exec(async () => {
      await mongo.pools.findOne({ uid: req.user.uid })
        .then(async (res) => { data = res.pools; });
    }).then(
      () => { res.status(200).send(JSON.stringify(data)); },
      err => {
        console.error(err);
        res.status(500).send({ error: err.message });
      });
    res.end();
  }).put("/pools", async (req, res) => {
    await mongo.exec(async () => {
      var sentPools = req.body;
      var dbPools = [];
      var dbPoolsObj = await mongo.pools.findOne({ uid: req.user.uid });
      for (var i = 0; i < dbPoolsObj.pools.length; i++)
        dbPools.push(dbPoolsObj.pools[i].name);
      for (var i = 0; i < dbPools.length; i++) {
        var dbPool = dbPools[i];
        if (!sentPools.includes(dbPool)) {
          await mongo.pools.updateOne(
            { uid: req.user.uid },
            { $pull: { pools: { name: dbPool } } }
          );
        }
      }
      for (var i = 0; i < sentPools.length; i++) {
        var sentPool = sentPools[i];
        if (!dbPools.includes(sentPool)) {
          var newPool = { name: sentPool, wallets: [] };
          await mongo.pools.updateOne(
            { uid: req.user.uid },
            { $push: { pools: newPool } }
          );
        }
      }
    }).then(
      () => { res.status(200); },
      err => {
        console.error(err);
        res.status(500).send({ error: err.message });
      });
    res.end();
  }).put("", async (req, res) => {
    const pool = req.query.pool
    await mongo.exec(async () => {
      await mongo.pools.updateOne(
        { uid: req.user.uid, "pools.name": pool },
        { $set: { "pools.$.wallets": req.body } }
      );
    }).then(
      () => { res.status(200); },
      err => {
        console.error(err);
        res.status(500).send({ error: err.message });
      });
    res.end();
  });
