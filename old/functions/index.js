require('firebase-admin').initializeApp();
const functions = require('firebase-functions');
const account = require('./account');
const assets = require('./assets');
const region = 'europe-west3';
let ssrServerServer;

module.exports = {
	ssrServer: functions.region(region).https.onRequest(async (request, response) => {
		if (!ssrServerServer) {
			functions.logger.info('Initialising SvelteKit SSR entry');
			ssrServerServer = require('./ssrServer/index').default;
			functions.logger.info('SvelteKit SSR entry initialised!');
		}
		functions.logger.info('Requested resource: ' + request.originalUrl);
		return ssrServerServer(request, response);
	}),

	account: functions.region(region).runWith({ minInstances: 0 }).https.onRequest(account),
	assets: functions.region(region).runWith({ minInstances: 0 }).https.onRequest(assets)
};
