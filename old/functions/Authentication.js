const admin = require('firebase-admin');

module.exports.Authentication = class {
  /**
   * Midware for token auth
   */
  async token(req, res, next) {
    const bearer = req.headers.token;
    if ((!bearer || !bearer.startsWith("Bearer "))
      && !(req.cookies && req.cookies.__session)) {
      res.status(403).send();
      return;
    }
    let idToken;
    if (bearer && bearer.startsWith("Bearer ")) {
      idToken = bearer.split("Bearer ")[1];
    } else if (req.cookies) {
      idToken = req.cookies.__session;
    } else {
      res.status(403).send();
      return;
    }
    try {
      const decodedIdToken = await admin.auth().verifyIdToken(idToken);
      req.user = decodedIdToken;
      next();
      return;
    } catch (error) {
      console.error(error);
      res.status(403).send();
      return;
    }
  }
}