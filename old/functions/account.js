const envFn = require('./env.json');
const express = require('express');
const cors = require('cors');
const { Mongo } = require('./tools');
const { Authentication } = require('./Authentication');
const mongo = new Mongo();
const auth = new Authentication();

const origin = [
  "https://poolance-crypto-monitor.web.app",
  "http://localhost:5173"][envFn.server];

module.exports = express().use(cors({ origin }), auth.token)
  .post("", async (req, res) => {
    await mongo.exec(async () => {
      await mongo.wallets.countDocuments({ uid: req.user.uid }).then(async (wallets) => {
        if (!wallets)
          await mongo.wallets.insertOne({ uid: req.user.uid });
      });
      await mongo.pools.countDocuments({ uid: req.user.uid }).then(async (pools) => {
        if (!pools)
          await mongo.pools.insertOne({ uid: req.user.uid, pools: [] });
      });
    }).then(
      () => { res.status(200); },
      err => {
        console.error(err);
        res.status(500).send({ error: err.message });
      });
    res.end();
  }).delete("", async (req, res) => {
    await mongo.exec(async () => {
      await mongo.wallets.deleteMany({ uid: req.user.uid });
      await mongo.pools.deleteMany({ uid: req.user.uid });
    }).then(
      () => { res.status(200); },
      err => {
        console.error(err);
        res.status(500).send({ error: err.message });
      });
    res.end();
  });
