const coinbaseUrl = 'https://api.coinbase.com/v2/exchange-rates?currency=';

async function coinbase(time, coin) {
	if (!time) throw new Error('No time to refresh prices given.');
	let price = 0;
	await fetch(coinbaseUrl + coin)
		.then((res) => res.json())
		.then((data) => {
			price = Math.round(data.data.rates.EUR * 100) / 100;
		})
		.catch((err) => {
			console.error(err);
		});
	return price;
}

async function apiTwoMiners(time, api) {
	if (!time) throw new Error('No time to refresh assets given.');
	let stats = { balance: 0, immature: 0 };
	await fetch(api)
		.then((res) => res.json())
		.then((data) => {
			stats = { balance: data.stats.balance, immature: data.stats.immature };
		})
		.catch((err) => {
			console.error(err);
		});
	return stats;
}

async function apiEthminer(time, api) {
	if (!time) throw new Error('No time to refresh assets given.');
	let stats = { balance: 0, immature: 0 };
	await fetch(api)
		.then((res) => res.json())
		.then((data) => {})
		.catch((err) => {
			console.error(err);
		});
	return stats;
}

async function apiMinexmr(time, api) {
	if (!time) throw new Error('No time to refresh assets given.');
	let stats = { balance: 0, immature: 0 };
	await fetch(api)
		.then((res) => res.json())
		.then((data) => {})
		.catch((err) => {
			console.error(err);
		});
	return stats;
}

export default {
	coinbase,
	apiTwoMiners,
	apiEthminer,
	apiMinexmr
};
