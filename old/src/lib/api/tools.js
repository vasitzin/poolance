import {
	createUserWithEmailAndPassword,
	signInWithEmailAndPassword,
	reauthenticateWithCredential,
	deleteUser
} from 'firebase/auth';

import { authG } from '$lib/store.js';

import db from '$lib/api/db.js';

let auth;
authG.subscribe((value) => {
	auth = value;
});

async function copyClip(text = '') {
	await navigator.clipboard.writeText(text);
}

async function logup(email, pass) {
	let user = null;
	await createUserWithEmailAndPassword(auth, email, pass)
		.then(async (userCredential) => {
			user = userCredential.user;
			await db.logup(await auth.currentUser.getIdToken());
		})
		.catch((error) => {
			console.error(error);
			throw error;
		});
	return user;
}

async function login(email, pass) {
	let user = null;
	await signInWithEmailAndPassword(auth, email, pass)
		.then((userCredential) => {
			user = userCredential.user;
		})
		.catch((error) => {
			console.error(error);
			throw error;
		});
	return user;
}

async function reauth(email, pass) {
	let user = null;
	await reauthenticateWithCredential(auth, email, pass)
		.then((userCredential) => {
			user = userCredential.user;
		})
		.catch((error) => {
			console.error(error);
			throw error;
		});
	return user;
}

async function remove(user) {
	const token = await auth.currentUser.getIdToken();
	await deleteUser(user)
		.then(async () => {
			await db.remove(token);
		})
		.catch((error) => {
			console.error(error);
			throw error;
		});
}

export default {
	copyClip,
	logup,
	login,
	reauth,
	remove
};
