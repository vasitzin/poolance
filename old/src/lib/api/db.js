import env from '$lib/env.json';
import { assetsG } from '$lib/store.js';

const server = [
	'https://europe-west3-poolance-crypto-monitor.cloudfunctions.net',
	'http://localhost:5839'
][0];

async function logup(token) {
	const options = {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			Token: `Bearer ${token}`
		}
	};
	await fetch(`${server}/account`, options).then((res) => {
		if (!res.ok) throw new Error(res.statusText);
	});
}

async function remove(token) {
	const options = {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
			Token: `Bearer ${token}`
		}
	};
	await fetch(`${server}/account`, options).then((res) => {
		if (!res.ok) throw new Error(res.statusText);
	});
}

async function loadData(token) {
	let assets = [];

	assetsG.set(null);
	const options = {
		headers: {
			'Content-Type': 'application/json',
			Token: `Bearer ${token}`
		}
	};
	await fetch(`${server}/assets`, options)
		.then((res) => {
			if (!res.ok) throw new Error(res.statusText);
			return res.json();
		})
		.then((data) => {
			for (var i = 0; i < data.length; i++) {
				var pool = data[i].name;
				assets.push({ pool, coins: [] });
				var url = '';
				for (var j = 0; j < data[i].wallets.length; j++) {
					var coin = data[i].wallets[j].asset;
					var wallet = data[i].wallets[j].wallet;
					url = env.pools[pool][coin].account.split('<$>');
					var account = `${url[0]}${wallet}${url[1]}`;
					url = env.pools[pool][coin].api.split('<$>');
					var api = `${url[0]}${wallet}${url[1]}`;
					assets[i].coins.push({ coin, wallet, account, api });
				}
			}
		})
		.catch(() => {
			assetsG.set([]);
		});
	assetsG.set(assets);
}

async function updatePools(token, pools) {
	const options = {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			Token: `Bearer ${token}`
		},
		body: JSON.stringify(pools)
	};
	await fetch(`${server}/assets/pools`, options)
		.then((res) => {
			if (!res.ok) throw new Error(res.statusText);
			return res.json();
		})
		.catch((err) => {
			console.error(err);
		});
}

async function updatePoolCoins(token, pool, coins) {
	const options = {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			Token: `Bearer ${token}`
		},
		body: JSON.stringify(coins)
	};
	await fetch(`${server}/assets?pool=${pool}`, options)
		.then((res) => {
			if (!res.ok) throw new Error(res.statusText);
			return res.json();
		})
		.catch((err) => {
			console.error(err);
		});
}

export default {
	logup,
	remove,
	loadData,
	updatePools,
	updatePoolCoins
};
