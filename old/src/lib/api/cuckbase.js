import { firebase } from '$lib/env.json';

import { initializeApp } from 'firebase/app';

import { getAuth, onAuthStateChanged, setPersistence, browserLocalPersistence } from 'firebase/auth';

import { appG, authG, userG } from '$lib/store.js';

import db from '$lib/api/db.js';

var initApp = initializeApp(firebase.cfg);
var initAuth = getAuth(initApp);

setPersistence(initAuth, browserLocalPersistence);

onAuthStateChanged(initAuth, async () => {
  var authCurrentUser = getAuth().currentUser;
  if (authCurrentUser) {
    userG.set(authCurrentUser);
    await db.loadData(await authCurrentUser.getIdToken())
      .catch(async (err) => { console.error(err); });
  } else {
    userG.set(null);
  }
});

appG.set(initApp);
authG.set(initAuth);

export default { init: () => { } };
