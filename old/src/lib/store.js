import { writable } from 'svelte/store';

/**
 * TODO desc
 */
export const themeG = writable("");

/**
 * TODO desc
 */
export const verticalG = writable(false);

/**
 * TODO desc
 */
export const slimG = writable(false);

/**
 * TODO desc
 */
export const appG = writable(null);

/**
 * TODO desc
 */
export const authG = writable(null);

/**
 * TODO desc
 */
export const userG = writable("pending");

/**
 * TODO desc
 */
export const assetsG = writable([]);

/**
 * TODO desc
 */
export const loginActivateDialog = writable(false);

/**
 * TODO desc
 */
export const reauthActivateDialog = writable(false);

/**
 * TODO desc
 */
export const poolFormActivate = writable(false);

/**
 * TODO desc
 */
export const coinFormActivate = writable(false);

/**
 * TODO desc
 */
export const coinFormPool = writable("");

/**
 * TODO desc
 */
export const coinFormCoins = writable([]);

/**
 * TODO desc
 */
export const refreshTwoMiners = writable({ coin: "all", time: new Date().valueOf() });

/**
 * TODO desc
 */
export const refreshCoinbase = writable({ coin: "all", time: new Date().valueOf() });