#!/bin/bash

IMG="poolance-front"
PRJ="/home/$USER/src"
DIR="/home/$USER"

do="sudo"
[ -f /usr/bin/doas ] && do="doas"

images=($(docker image ls | grep "$IMG" | wc -l))
if [ $images -lt 1 ]; then
  docker build --build-arg DOCKERER=$USER -t $IMG . || exit 99
fi

$do docker run -it -v ~/.bash_history:$DIR/.bash_history -v .:$PRJ -w $PRJ --rm $IMG $@
